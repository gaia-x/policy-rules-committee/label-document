# Changelog

## 2024 November release (24.11)

- Criteria [1.3.2](../criteria_cloud_services/#P1.3.2): only allow ISO 3166-2 information for headquarter localisation
- Criteria [3.1.18](../criteria_cloud_services/#P3.1.18): add CISPE as permissible standard
- Criteria [3.1.18](../criteria_cloud_services/#P3.1.18), [3.1.19](../criteria_cloud_services/#P3.1.19): for Label level 2 and 3, allow declarations until additional permissible standards are approved by the Gaia-X Association
- Editorial changes

## 2024 June release (24.06)

- The Compliance Document is the new name of the PRC specifications document, which in the previous version was named as Policy Rules Conformity Document (PRCD).
- Consistent use of "Compliance scheme" instead of conformity, labels, and other variations.
- Replace "Conformity"/"Basic Conformity" by "Gaia-X Standard Compliance".
- Update of the Gaia-X Trust Anchors chapter with the inclusion of the decision flowchart to determine which type of Trust Anchor must be defined for a given criteria and with the requirements for TSPs (Trust Service Providers) and the selection process for CABs.
- Introduction of the new chapter "List of Gaia-X Conformity Assessment Bodies" where all CABs, which are accredited to attest conformity against a permissible standard by the respective organizations body are listed. 
 - Update of the chapter Gaia-X Compliance Criteria for Cloud Services, with the detail of the required evidence for the first two compliance levels (Declaration evidence). 
- Update of the chapter "Proposed Compliance for Data Exchange Services" with the specification of the conformity assessment required for the different schemes. 

## 2024 April release (24.04)

- The PRCD is a combination of the previous Policy Rules and Labelling Document (PRLD) and Trust Framework, which are now obsolete. 
- New "Executive Summary" chapter. 
- New chapter on the Process description for how to become a Gaia-X conformant user.
- New chapter on the Self-Declaration of Conformity.
- Definition of Conformity and related criteria.
- New criteria and attributes (mandatory and optional) on "Sustainability".
- New chapter on Proposed Data Exchange Criteria.
- Update and refinement of Permissible and Example Standards for Gaia-X Labels.