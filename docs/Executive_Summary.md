# Executive Summary

The Gaia-X Policy Rules define high-level objectives safeguarding the added value and principles of the Gaia-X Ecosystem.
To allow for validation, the high-level objectives are underpinned by the Gaia-X Labelling Criteria and the Gaia-X Trust Framework.

The intent of the Gaia-X Policy Rules is to identify clear controls to demonstrate the core European values of Gaia-X: openness, transparency, data protection, security, and portability.
The Gaia-X Standard Compliance level defines the minimal set of requirements to be able to participate in the Gaia-X Ecosystem.
The optional Label levels define additional criteria and conformance-ensuring measures such as certificates, to achieve additional levels of assurance and trust, with a focus on European values and based on EU/EEA legislation.
These initial Labels can be extended, and additional Labels can be added in the future, to accommodate for sectorial or geographical needs.
Compliance with these Gaia-X Policy Rules objectives can be achieved via compliance with established standards, certifications, and codes of conduct.

At this stage, the document lists the normative high-level objectives for service offering providers in the following categories: contractual framework, data protection, cybersecurity, European control, and sustainability.
The document also defines the mandatory and suggested optional attributes to be used to describe Participants, Services and Resources in the Gaia-X Ecosystem.
Furthermore, the document includes a work-in-progress on the requirements towards data exchange services.

The fulfilment of the high-level objectives can be realized in various levels of conformance. 
The Gaia-X Standard Compliance level includes the set of rules that define the minimum baseline to be part of the Gaia-X Ecosystem. Those rules ensure a common governance and the basic levels of interoperability across individual ecosystems while letting the users in full control of their choices.
In other words, the Gaia-X Ecosystem is the virtual set of participants and Service Offerings following the Gaia-X Compliance requirements. 

The Trust Framework uses verifiable credentials and linked data representation to build a FAIR (Findable, Accessible, Interoperable, and Reusable) knowledge graph of verifiable claims from which additional trust and composability indexes can be automatically computed.
The Labelling Framework extends upon the Gaia-X Standard Compliance level and makes use of verifiable credentials to extend the Trust Framework.
Thus, it is ensured that all information required to make a qualified choice between different services is available in a consistent and standardized machine-readable form. 

The Labelling Framework itself is further detailed and translated into concrete criteria and measures in this document.
The criteria list brings together the policies and requirements from the various Gaia-X Committees – Policy Rules Committee, Technical Committee, and Data Services & Business Committee – along with comprehensive assessments to ensure that these requirements can be met.
It allows for further differentiation between services, which is necessary for users wanting to find services for different purposes and based on different needs. 

The Gaia-X Labelling Framework is designed using a set of core principles, starting from the high-level objectives which are refined by the labelling criteria.
The Labels require consistency among the Gaia-X Ecosystem, scalability and extensibility, composability and modularity mapping, referencing of existing standards, self-assessment and Conformity Assessment Bodies (CAB).

Gaia-X distinguishes 3 levels of Labels, starting from Label Level 1 (the lowest), up to Label Level 3 (the highest), which represent different degrees of compliance with regard to the goals of transparency, autonomy, data protection, security, interoperability, portability, sustainability, and European Control.