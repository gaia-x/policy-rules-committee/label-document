# Gaia-X Power of Attorney format

To adapt to various organisation structures and introduce a mean for a party to delegate rights to another party, a power of attorney is useful.

A Gaia-X Power of Attorney is a machine readable, structured and signed document that comprises at a minimum the following information attributes:

- Power of Attorney ID, as unique identifier.
- Principal ID, as unique identifier for the Participant that signs the power of attorney.
- Principal cryptographic material reference, to enable the verification of the signature of the power of attorney.
- Authorised Participant ID, as unique identifier for the Participant to whom the powers are granted.
- Authorised Participant cryptographic material reference, to enable the verification of the signature of future claims signed by the Authorised Participant.
- A list of granted powers, either as text or preferably in a machine-readable format using a Domain Specific Language (DSL) like ODRL.
- Issuance date on which the power of attorney was issued.
- Validity start and end dates on which the power of attorney enters into force and will expire.


!!!tip
    In technical terms, a Gaia-X Label is a [W3C Verifiable Credential](https://www.w3.org/TR/vc-data-model-2.0/) and the JSON schema covering the above functional expectations is available via the [Gaia-X Registry](https://registry.gaia-x.eu/).