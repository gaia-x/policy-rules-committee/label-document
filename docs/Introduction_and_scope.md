## Preface

Gaia-X Compliance, depending on the specific conformity assessment scheme, can be assessed by technical means and via  compliance with established standards, certifications, and codes of conduct.
The addition and maintenance of these standards will be defined in this document. Where such tools are not available or approved to demonstrate such compliance, specific methodologies can be further developed and agreed upon within Gaia-X to be included in the attestation of Service Offerings.

In the cybersecurity section we refer, when it is possible, to the current discussions on the European cybersecurity certification scheme for cloud services (the EU Cloud Services Scheme or EUCS).
When the EUCS is finalised, Gaia-X may consider adapting the objectives in this document.

Please note that, in general, full adherence to applicable local legislation (e.g., in areas such as data protection and security) is a prerequisite and thus not waived or affected by the following criteria.

It is worth pointing out that participation within Gaia-X by providing Gaia-X compliant services shall not prevent any Provider from also providing non-Gaia-X compliant Service Offerings outside the Gaia-X Ecosystem.

This document is a work in progress, i.e. it will be further worked on to evolve towards a fully clear and complete specification of the Gaia-X Compliance criteria. 

Gaia-X will update this document on a regular basis.

Following the publication of the Compliance Document (CD), the previous deliverables of the Policy Rules Committee (Gaia-X Policy Rules and Labelling Document, Gaia-X Trust Framework) are obsolete. 

## Design Principles for Gaia-X Standard Compliance and Labels

The Gaia-X Compliance is designed using a set of core principles, starting from the Standard Compliance scheme, which is refined in the Label schemes.

| property                                 | Standard Compliance | Level 1 | Level 2 | Level 3 |
|------------------------------------------|------------|---------|---------|---------|
| Declaration of Service or Product        | ✔️         | ✔️      | ✔️     | ✔️      |
| Signed with verified method (e.g. eIDAS) | ✔️         | ✔️      | ✔️     | ✔️      |
| Automated validation by GXDCH            | ✔️         | ✔️      | ✔️     | ✔️      |
| Automated verification by GXDCH\*         | ✔️         | ✔️      | ➕      | ➕     |
| Data Exchange Policies                   | ✔️         | ✔️      | ✔️      | ✔️     |
| Certified Label Logo                     |            | ✔️      | ✔️      | ✔️     |
| Data protection by EU legislation        |            | ✔️      | ✔️      | ✔️     |
| Manual verification by CAB               |            |         | ✔️      | ✔️      |
| Provider Headquarter within EU           |            |         |         | ✔️      |

\*: *not all criteria can be automated.

➕: means automated verification of the evidence issuer (Standard & CAB)

### Consistency among the Gaia-X Ecosystem

Gaia-X Compliance reflect the essence of our objectives and concepts. They represent the results of decisions and deliverables introduced by the various Gaia-X Committees and approved by the Gaia-X Board of Directors.
The Gaia-X Compliance criteria are always in line with the corresponding concepts and specifications as defined by Gaia-X.
  
### Scalability and extensibility

Based on the four assessment scheme of Gaia-X Standard Compliance and the three (3) Label Levels, further Gaia-X Labels can be created to fit new needs, in particular using extension profiles for country and domain-specific requirements.
Extension profiles can also leverage the Label schemes by adding and defining on-top requirements for particular purposes.
To ensure the impact and consistency of Gaia-X Compliance, new labels and extensions have to be authorized by the Gaia-X Board of Directors.

### Composability and modularity

Gaia-X Compliance schemes are logical groupings of composable service attributes.
This results in particular in the assignment of a common set of policies, technical requirements and data space criteria to one or multiple of four schemes.
At the same time, Gaia-X Compliance is based upon existing schemes, certifications, and tested and approved codes of conduct where possible to allow the reuse of established standards and thereby simplify the process.
Only in areas where no standard has been identified Gaia-X will introduce its own set of attributes and processes to verify the information given.

### Standards, self-assessment and Conformity Assessment Bodies (CABs)

Gaia-X Compliance do not normatively reference external documents which are not yet approved (for example the current proposal of the EUCS).
Whenever such external documents are approved, Gaia-X may consider adapting its criteria in accordance with them.

Compliance with criteria can be declared by declaration or certification (supported by external Conformity Assessment Bodies), as defined later in this document.

Gaia-X Service Offerings are defined by Provider-generated credentials which include claims that will be validated and verified to prove compliance to the different schemes.
The proof of validation of a claim will be also issued as a verifiable credential.
The Verifiable Credential can either be issued by a Provider or a CAB directly or it can be created by a trusted Verifiable Credential issuer based on existing documentation (like a signed PDF or paper document).

The Verifiable Credential includes the entity asserting the validity of the claim; the list of trusted Verifiable Credentials issuers is maintained in the Gaia-X Registry.

Users at any time can query the attestation of the Service Offering and for each claim extract the entity and the result of the assessment.

Conformity Assessment Bodies (CABs): Gaia-X reserves its right to choose its own CAB per criteria.
A new detailed document will be issued on the process of choosing the relevant CAB.
Where the criteria lack reference to established standards, Gaia-X defines a dedicated Assessment Process including a process to appoint adequate CABs (Conformity Assessment Bodies), following internationally recognized good practices, including impartiality, comparability, reliability and accessibility.

### Mapping and Referencing of existing standards
It is intended that this document will provide for each criterion a detailed mapping and references to existing standards and certification schemes.
This mapping and referencing shall be as detailed as possible, saying that, instead of a generic identification of a standard, the relevant sections in such standards shall be identified. 

**Example Standard**: This document may provide so-called "Example Standards".
Example Standards shall identify potential means of implementation.
Gaia-X strives to refer to existing standards and controls to the extent possible.
Re-drafting shall be prevented. Nonetheless, Gaia-X and referenced standards may have a different focus and high-level objective.
Example standards shall provide for possibilities how criteria may be implemented.
Implementation as provided by such standards is not mandatory, and it is required to comply with any such standards.
Gaia-X will provide additional notes, if significant differences are identified.
Example Standards shall especially help in evaluating compliance with Gaia-X, as Example Standards can be considered "implementation guidance". 

_Note: Example Standards will be added after following a thorough assessment by the Gaia-X Working Groups maintaining this document. Such assessment shall follow a transparent process. No Example Standards shall be listed, prior to such process is defined and applied in the determination. The process shall foresee that third-party standards may reach out to Gaia-X and suggest being enlisted._

**Permissible Standard**: This document may provide so-called "Permissible Standards".
Permissible Standards shall identify standards respectively requirements/controls within such standards, where implementation shall be considered prima facie evidence of conformity with the related Gaia-X criterion.
_Note: Permissible Standards can only be added following a thorough assessment by the Gaia-X Working Groups maintaining this document. Such assessment shall follow a transparent process. No Permissible Standards shall be listed, prior to such process is defined and applied in the determination. The process shall foresee that third-party standards may reach out to Gaia-X and suggest being enlisted. The process shall cover both, the material requirements as well as the overarching conformity assessment programme, i.e., the means by which such Permissible Standard determines whether the subject of such assessment is indeed conformant/compliant._

## Proof of Concept / Bootstrapping

### Conformity Assessment Programme and Assessibility

The criteria listed in this document must be and remain assessible at all times.
Gaia-X is currently developing accompanying documents outlining the overarching conformity assessment programme and process. 

Also, this document will further evolve to enhance the assessibility of its criteria to the extent necessary, e.g. where Gaia-X will not or cannot rely on existing standards and conformity assessment programmes.

Gaia-X anticipates that the requirements outlined in this document are assessible.
If comparability of assessment results cannot be guaranteed, or if ambiguities exist, Gaia-X may have to adapt these rules, criteria, or assessment mechanisms in future versions.

In this vein and as mentioned elsewhere in this document, Gaia-X will monitor current regulatory developments as well as developments in the field of standards and conformity assessment programmes.
Whilst Gaia-X may consider existing drafts as inspiration, Gaia-X does not endorse any such drafts.
Likewise, Gaia-X remains in control of whether to adapt its requirements to future iterations of any such external developments. 

### Federation of Verification

Gaia-X Labels are issued according to determined criteria and assessments in a federated manner.
The concept of modularity also allows Gaia-X to reuse existing certifications for the underlying service attributes whenever possible, hence reducing the cost and complexity of embracing Gaia-X labelling, especially for existing, already certified, services.
Assessment Processes defined by Gaia-X itself will also be based on a federation of responsibilities.

### Further design principles

The modularity concept requires Gaia-X labelling criteria to describe rather high-level objectives as the detailed requirements are further described in the corresponding standards that are acknowledged. 
As of today, Gaia-X Labels are issued to a specific Service Offering unless stated otherwise. 

## Extendibility of Gaia-X Compliance

Gaia-X Compliance applies to **all** Gaia-X Service Offerings.
And there shall be a Gaia-X Credential for **all** the entities defined as part of the Gaia-X Conceptual model:

- Participant including Consumer and Provider
- Service Offering
- Resource

The Gaia-X Compliance scheme can be extended by an ecosystem as detailed in the Gaia-X Architecture Document.

## Period of Validity

The targeted updating period of the document is eighteen (18) months. 
Exceptionally, in case of changes that have become appropriate under applicable laws or standards impacting this document or the Gaia-X Compliance requirements, an update can be made earlier subject to a decision by the Gaia-X Board of Directors.

Upon revisions of the Gaia-X Compliance document, the participants will have the choice of adapting their compliance to the revised requirements or remaining qualified under the former requirements, for a maximum duration of twelve (12) months from the entry into force of the revised Gaia-X Compliance requirements.
Exceptionally, in case of changes that have become appropriate under applicable laws or standards impacting the Gaia-X Compliance document, the Gaia-X Board of Directors can determine a grace period that deviates from the maximum twelve (12) months term, when relevant in view of the applicability of the changes of the applicable laws or standards.
