# Gaia-X Trust Anchors

Gaia-X Trust Anchors are bodies, parties, i.e., Conformity Assessment Bodies or technical means accredited by the bodies of the Gaia-X Association to be parties eligible to issue attestations about specific claims.

For each accredited Trust Anchor, a specific [scope of attestation](https://www.iso.org/obp/ui/#iso:std:iso-iec:17000:ed-2:v2:en:term:7.4) is defined.

The Trust Anchors are not necessarily Root Certificate Authorities as commonly understood, but they can be relative to different properties in a claim.

## Overall decision flowchart

The decision flowchart below is used to determine what type of Trust Anchor must be defined for a given criteria objective.

![](figures/Gaia-X_Conformity_Schema_process.png)


## Trust Anchors

### Signee's role

In the Gaia-X Ontology, for specific attributes which are linked or dependent from each other, a criteria can mandate that an attribute must be signed by the same issuer - or signee - of another attribute.

For example, in the [Gaia-X Trust Framework 22.10](https://docs.gaia-x.eu/policy-rules-committee/trust-framework/22.10/), it is mandatory for the information whether or not a Data Product contains PII that the attribute `dataProduct.containsPII` is signed by the Producer of this Data Product `dataProduct.produceBy`.

### Trust Service Provider

By default, for the claims to be legally relevant, all claims must be signed with one or more cryptographic material which can be traced back to a Trust Anchor, which is in most case a [Trust Service Provider (TSP)](https://en.wikipedia.org/wiki/Trust_service_provider).

The Trust Service Providers (TSP) accredited by Gaia-X must be entities issuing cryptographic material based on documented Know Your Business/Know Your Customer [(KYB/KYC)](https://en.wikipedia.org/wiki/Know_your_customer) processes.
Those processes must verify the identity of the party requesting the digital certificate associated to the cryptographic material, such as, and not limited to:

- Business registration or license verification
- Physical address verification
- Phone number verification



The non-exclusive list of accepted Trust Service Providers belong to these categories:
- EEA 🇪🇺, Iceland 🇮🇸, Liechtenstein 🇱🇮, Norway 🇳🇴: eIDAS [Regulation (EU) No 910/2014](https://eur-lex.europa.eu/legal-content/EN/TXT/?uri=uriserv:OJ.L_.2014.257.01.0073.01.ENG). ([Homepage](https://esignature.ec.europa.eu/efda/tl-browser/#/screen/home), [Trusted Data Source](https://ec.europa.eu/tools/lotl/eu-lotl.xml))
- India 🇮🇳: [eMuhdra](http://www.e-mudhra.com/) ([Homepage](https://cca.gov.in/e_mudhra.html), [Trusted Data Source](https://cca.gov.in/display_licensed_ca.php))
- South Korea 🇰🇷: [KTNET](https://www.tradesign.net/service/introduce/apply) ([Homepage](https://trustesign.kisa.or.kr/intro/cert))
- United Arab Emirates (UAE) 🇦🇪: [PASS](https://uaepass.ae/) ([Homepage](https://tdra.gov.ae/-/media/About/LEGAL-REFERENCES/LAW/LAW-English/Electronic-Transactions-and-trust-sevices-law-AR.ashx))

To have a global reach, and only if there is no alternative specified in the Gaia-X Registry for the country of the business registration, Gaia-X allows the use of Extended Validation (EV) Secure Sockets Layer (SSL) certificate to sign attributes. ([Homepage](https://wiki.mozilla.org/CA/Included_Certificates), [Trusted Data Source](https://ccadb-public.secure.force.com/mozilla/IncludedCACertificateReportPEMCSV))


The accepted TSP categories are determined within the Gaia-X Compliance document, while the detailed list of valid TSP belonging to these categories resides in the [Gaia-X Registry](https://registry.gaia-x.eu/).

## Trusted Data Sources and Notaries

When an accredited Trust Anchor is not capable of issuing cryptographic material nor signing claim directly, the Gaia-X Association accredits one or more Notaries which convert "not machine readable" proofs into "machine readable" proofs.
A Gaia-X Notary must be a Gaia-X participant capable of translating an unsigned evidence to a signed machine readable evidence.
For signing, the Gaia-X Notary must use a cryptographic material issued by a Trust Anchor.

Notaries perform validations and issue attestations based on objective [evidences](https://www.w3.org/TR/vc-data-model-2.0/#evidence) from Trusted Data sources.
The Verifiable Credentials issued by the Notaries contain the evidences of the validation process.

The following Trusted Data Sources have been accredited by Gaia-X and are currently used by the Gaia-X Notary Service to validate and issue attestations on the Participant's Legal Registration Number:

- `EORI`: the European Commission [API](https://ec.europa.eu/taxation_customs/dds2/eos/validation/services/validation?wsdl).
- `leiCode`: the Global Legal Entity Identifier (GLEIF) [API](https://www.gleif.org/en/lei-data/gleif-api?cachepath=fr%2Flei-data%2Fgleif-api)
- `local`: the OpenCorporate [API](https://api.opencorporates.com/)
    - the returned claim will also contain information about `headquarterAddress.countryCode`
- `vatID`: for the European member states or North Ireland, the VAT Information Exchange System (VIES) [API](https://ec.europa.eu/taxation_customs/vies/checkVatTestService.wsdl)
    - the returned claim will also contain information about `headquarterAddress.countryCode` 

The accepted Trusted Data Source categories and Notaries are determined within the Gaia-X Compliance document, while the detailed list of valid Trusted Data Sources and Notaries resides in the [Gaia-X Registry](https://registry.gaia-x.eu/).

## CAB, "Equivalence CAB", "Gap CAB"

All [CAB](https://www.iso.org/obp/ui/#iso:std:iso-iec:17000:ed-2:v2:en:term:4.6)s which are accredited to attestate conformity against a permissable standard by the respective oganizations body are accepted by Gaia-X.

An "Equivalence [CAB](https://www.iso.org/obp/ui/#iso:std:iso-iec:17000:ed-2:v2:en:term:4.6)" is an identified entity approved by Gaia-X to verify that one or more issued [certifications](https://www.iso.org/obp/ui/#iso:std:iso-iec:17000:ed-2:v2:en:term:7.6) cover the entirety of a given criteria scope.

A "Gap [CAB](https://www.iso.org/obp/ui/#iso:std:iso-iec:17000:ed-2:v2:en:term:4.6)" is an identified entity approved by Gaia-X to issue a [certification](https://www.iso.org/obp/ui/#iso:std:iso-iec:17000:ed-2:v2:en:term:7.6) for a scope identified as not covered by an "Equivalence CAB".

The full list of valid CAB, "Equivalence CAB", "Gap CAB" is kept up-to-date and made available via the [Gaia-X Registry](https://registry.gaia-x.eu/).

![CABSchema](image-2.png)

  | Scenario  |  The certification covers, at least, the entirety of the criterion’s scope. | Overlap of the various certification’s scope to be assessed by an equivalence CAB.|The certification(s) don’t cover the entirety of the criterion’s scope, requiring the gap to be assessed separately.  |
 | -------- | --------- | -------- | ---------- | 
 | Trust Anchors type | List of CAB per certification scheme. |List of Gaia-X equivalence CAB | List of Gaia-X gap CAB
|
## How to use CAB certifications ?

CAB certifications issued by a CAB listed in the [GAIA X Registry](https://registry.gaia-x.eu/) can be used to validate all criterions fully compliant covered by CAB certificate's perimeter as described in this document.

![CABcertifusageschema](image-5.png)

## GAP CAB Approval Process

The following defines the approval process for GAP CABs (if the CABs are supposed to issue verifiable credentials for Gaia-X Criteria not fully covered by Permissible Standards) to be approved for the verification and final decision. These criteria and proofs will ensure the required competence, a common understanding of the relevant documents, requirements, and procedures for the Gaia-X Labelling.

If Gaia-X criterion are all covered by several international Permissible Standards, this process may not be needed anymore and removed from this document.

![GAPCABProcessOverview](figures/GAPCABProcessOverview.png)

### Key GAIA-X commitments for the Approval process

**Transparency**

All processes and approval criteria are publicly disclosed (e.g. on the Gaia-X website), ensuring a transparent and fair approval process which creates reliable trustfullness.

**Impartiality**

The PRC ensures impartiality for the approval process in line with ISO/IEC 17011 principles.

**Competence and experience**

The PRC assures that an assessment of competence and experience is performed on Gap CAB applicant in order to demonstrate the required expertise in the domains of Gaia-X criteria and on other permissible standards in line with ISO/IEC 17011 principles.

**Quality Assurance**

The PRC assures an ongoing quality assurance mechanism, including periodic surveillance assessment and reviews of CABs' continuous improvement plans.

### Application

Entities, interested in becoming an approved/listed CAB for Gaia-X Labelling, submit an application form to Gaia-X AISBL including documented proofs of applicable criteria (e.g. qualification, experience, and impartiality).

Along with the application, a checklist that correlates with the criteria for CABs should be provided to aid in documenting the review.

### Initial Evaluation

The PRC reviews the application for completeness and preliminary adherence to the criteria. Areas, where the applicant does not fully meet the criteria, are identified.

### Assessment

If the criteria are only partial met, additional physical or virtual assessments might be necessary on behalf or by the PRC to validate the information provided and to ascertain the CAB's adherence to criteria.

### PRC Approval/Rejection

The final approval is based on the evaluation of the submitted documents and if conducted, the report about the physical or virtual assessments.

After positive evaluation, the CAB will get an Approval Certificate (Statement of Approval) by Gaia-X AISBL and be listed on the Gaia-X Registry.

In case of negative evaluation, the CAB will get a written notification detailing the reasons for refusal to approve and list them. This will include specific references to the applicable criteria for approval.

### Criteria for the approval of CABs

This section defines the criteria that CABs shall fulfil to carry out the conformity assessments for verifiable credentials. These criteria and respective proofs will ensure the required competence for all conformity assessments related to the Gaia-X criteria which are relevant for the Gaia-X Labels.

- If a CAB is not yet approved/accredited by the responsible Approval Body for a Permissible Standard, the CAB shall proof adherence to the Gaia-X Criteria as outlined in section "Criteria of approval for CAB that are not yet approved/accredited by an Approval Body for a Permissible Standard" below.

- (Most probably) If a CAB is already active as a CAB for one or more of the Permissible Standards listed in this document and thus is approved/accredited by the responsible Approval Body to issue the corresponding verifiable credential, additional proofs are required with respect to technical competence of the personnel, due to the various domains of the Gaia-X Criteria not covered by each of the Permissible Standards. Which specific proofs are required per Permissible Standard is outlined in section "Proofs of suitability for approval as CAB to remediate gaps" below.

#### Criteria of approval for CAB that are not yet approved/accredited by an Approval Body for a Permissible Standard

##### Organisational structure and governance

**Legal status**

Criteria: Shall be a legally recognized entity capable of entering into contracts and assuming liability.
Proof: Current extract from the commercial register

**Impartiality and independence**

Criteria: Policies should be in place to prevent commercial, financial, or other pressures from compromising impartiality.

Proof: Internal impartiality policy, organizational chart, documented procedures for dealing with conflicts of interest or accreditation certificate issued by a recognized accreditation body listed as a member of the International Accreditation Forum (IAF) or verification of respective statements in the audit firms’ Transparency Report issued in accordance with the requirement set forth in European statutory audit regulations (Article 13 of Regulation (EU) 537/2014) or an equivalent approval. 

##### Competence and personnel

**Technical competence**

Criteria: Shall have personnel with expertise in the domains of the Gaia-X Criteria.

Proof: Records about existing approvals for Permissible Standards or appropriate evidence to proof the competence with respect to the domains of the Gaia-X Criteria.

**Training**

Criteria: Should have a continuous professional development programme in place.

Proof: Training plans, attendance records, training content.

**Participation in Gaia-X meetings for experience exchange and training**

Criteria: Shall participate in annual Gaia-X meetings for experience exchange and training to ensure a common understanding of all rules and procedures within Gaia-X Labelling and a fair competition between the Equivalence CABs.

Proof: Training plans, attendance records, training content.

##### Assessment process management

**Process documentation**

Criteria: Shall have comprehensive and up-to-date documentation for all assessment activities and follow the publication and updates by Gaia-X.

Proof: Process documentation, SOPs (Standard Operating Procedures).

**Confidentiality**

Criteria: Measures should be in place to protect confidential information.

Proof: Privacy policies, NDAs with employees and subcontractors.

**Transparency**

Criteria: Processes, criteria and results should be publicly available unless restricted by law or confidentiality.

Proof: Published procedures, public records of statements of conformity issued.

##### Quality assurance and continuous improvement

**Internal audits**

Criteria: Should conduct regular internal audits to assess compliance with processes and standards.

Proof: Internal audit reports, corrective action plans.

**Management reviews**

Criteria: Should periodically review the effectiveness of the quality management system.

Proof: Minutes of management review meetings, action items and follow-up reports.

##### Subcontracting and outsourcing

**Responsibility**

Criteria: Remains responsible for all outsourced activities and shall ensure that subcontractors for labeling issuing activities meet the same quality criteria.

Proof: Subcontractor agreements, quality control checks on subcontractor output.

**Quality Control**

Criteria: Shall have a monitoring system to assess the performance of subcontractors.

Proof: Monitoring reports, subcontractor performance metrics.

##### Records and documentation

**Data management**

Criteria: Shall securely manage all records and documentation related to labelling activities.

Proof: Data management policies, data security protocols.

**Retention policy**

Criteria: Shall have a documented retention policy in accordance with relevant laws and regulations.

Proof: Document retention policies, compliance audits.

##### Monitoring and control

**Periodic reviews**

Criteria: Shall conduct periodic surveillance reviews to ensure that Gaia-X Service Offerings, for which Gaia-X Statements of Conformity were issued, continue to meet Gaia-X criteria.

Proof: Surveillance review reports, re-assessment records.

**Corrective actions**

Criteria: Shall have processes to address non-compliance, ranging from corrective action plans to withdrawal of Gaia-X Statements of Conformity.

Proof: Corrective action plans, records of enforcement actions.

##### Appeals and complaints

**Appeals procedures**

Criteria: Shall provide a mechanism for organizations to appeal Gaia-X Labelling/registration decisions.

Proof: Documented appeals process, records of appeals handled.

**Complaints handling**

Criteria: Shall have a process for receiving and resolving complaints about its labelling activities.

Proof: Documented complaints procedure, log of complaints received, and action taken.

#### Proofs of suitability for approval as CAB to remediate gaps 

**CISPE.cloud**

No additional proof required.

**EU Cloud Code of Conduct**

No additional proof required.

**BSI C5**

- Latest Transparency Report issued in accordance with the requirement set forth in European statutory audit regulations (Article 13 of Regulation (EU) 537/2014) does not indicate material deficiencies with respect to the criteria in section "Criteria for the approval of CABs".
- Entity has performed at least 2 conformity assessments for Cloud Service Providers in accordance with the programme within 12 months prior to the application (see section "Application") and can state respective references. 

**TISAX**

Entity has performed at least 2 conformity assessments for Cloud Service Providers in accordance with the programme within 12 months prior to the application (see section "Application") and can state respective references.

**AICPA**

- Latest Transparency Report issued in accordance with the requirement set forth in European statutory audit regulations (Article 13 of Regulation (EU) 537/2014) does not indicate material deficiencies with respect to the criteria in section "Criteria for the approval of CABs".
- Entity has performed at least 2 conformity assessments for Cloud Service Providers in accordance with the programme within 12 months prior to the application (see section "Application") and can state respective references. 

**ISO/IEC 27001**

- Entity has performed at least 2 conformity assessments for Cloud Service Providers in accordance with the programme within 12 months prior to the application (see section "Application") and can state respective references.
- Entity has appropriate knowledge and experience of GDPR

**CCM v4**

No additional proof required.

#### Additional proofs for the criteria on competence and personnel for CABs to remediate gaps

**CISPE.cloud**

No additional proof required.

**EU Cloud Code of Conduct**

No additional proof required.

**BSI C5**

Records for a minimum of 24h of structured training on GDPR requirements for personnel assigned to engagements to issue verifiable credentials with respect to the Gaia-X Criteria in the domain Data Protection. 

**TISAX**

Records for a minimum of 24h of structured training on GDPR requirements for personnel assigned to engagements to issue verifiable credentials with respect to the Gaia-X Criteria in the domain Data Protection.

**AICPA**

If conformity assessments comprised the “Privacy” category of the Trust Services Criteria: no additional proof required.
Otherwise: Records for a minimum of 24h of structured training on GDPR requirements for personnel assigned to engagements to issue verifiable credentials with respect to the Gaia-X Criteria in the domain Data Protection.

**ISO/IEC 27001**

Records for a minimum of 24h of structured training on GDPR requirements for personnel assigned to engagements to issue verifiable credentials with respect to the Gaia-X Criteria in the domain Data Protection.

**CCM v4**

Records for a minimum of 24h of structured training on GDPR requirements for personnel assigned to engagements to issue verifiable credentials with respect to the Gaia-X Criteria in the domain Data Protection.

