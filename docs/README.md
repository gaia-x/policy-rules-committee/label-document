# Editorial Information

## Publisher

Gaia-X European Association for Data and Cloud AISBL  
Avenue des Arts 6-9  
1210 Brussels  
www.gaia-x.eu

## Authors

GAIA-X European Association for Data and Cloud

## Contact

<https://gaia-x.eu/contact/>

## Other format

For convenience a PDF version of this document is generated [here](pdf/document.pdf).

## Copyright notice

©2024 Gaia-X European Association for Data and Cloud AISBL

This document is protected by copyright law and international treaties. This work is licensed under a [Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License](https://creativecommons.org/licenses/by-nc-nd/4.0/). Third-party material or references are cited in this document.

![cc by-nc-nd](figures/by-nc-nd.eu.png)
