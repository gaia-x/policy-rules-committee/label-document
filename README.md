# Gaia-X Compliance Documentation

This repository contains the compliance documentation for Gaia-X.

The output of this repository is published here <https://gaia-x.gitlab.io/policy-rules-committee/compliance-document/>
