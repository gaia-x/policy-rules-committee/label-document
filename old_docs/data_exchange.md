# Rules and Labelling Criteria for Data Exchange

This section includes an initial set of basic criteria for Data Exchange initiatives. This is works in progress, open for feedback. No Label levels are anticipated for this section at this stage, therefore only a Basic Conformity level is envisaged.

## Data Exchange services

In Gaia-X, `Data` are at the core of Data Exchange Services. `Data` are furnished by `Data Producers` (for instance data owners or data controllers in GDPR sense, data holder in EU data acts sense, etc.) to `Data Providers` who compose these data into a `Data Product` to be used by `Data Consumers`.

Further details of all the terms used in this section are defined in the Data Exchange chapter of the [Architecture](https://docs.gaia-x.eu/technical-committee/architecture-document/latest/) document and to keep definitions consistents across documents and versions, those definitions will not be duplicated here.

### Criterion P6.1.1

**objectives**: `Data` are furnished by `Data Producers` to `Data Providers` who compose these data into a `Data Product` to be used by `Data Consumers`. `Data Producers`,`Data Providers` and `Data Consumers` must be Participant with Gaia-X conformant descriptions.

Basic Conformity: mandatory declaration

Assessing Entity: Gaia-X AISBL or mandated entity

Assessment Process: self-assessment

### Criterion P6.1.2

**objectives**: Each `Data Product` description must contain a `Data License` defining the usage policy for all data in this Data Product. The `Data Provider` is accountable of the `Data Product` description declarations.

Basic Conformity: mandatory declaration

Assessing Entity: Gaia-X AISBL or mandated entity

Assessment Process: `Data Provider`

### Criterion P6.1.3

**objectives**: The Data Exchange Service must enable the `Data Provider`, `Data Producers` and the `Data Licensors` to define clear rules - permissions, prohibitions, duties - and consent (e.g. consent as per GDPR, authorization as per EU acts on data, permissions as per the EU Finance Data Access regulation, etc...).

Basic Conformity: mandatory declaration

Assessing Entity: Gaia-X AISBL or mandated entity

Assessment Process: self-assessment

### Criterion P6.1.4

**objectives**: A `Data Usage Agreement` (DUA), including usage terms and conditions associated with these data, is signed by both `Data Producer` and `Data Provider`, and gives the `Data Provider` the legal authorization to use these data in accordance with the specified constraints. 

Basic Conformity: mandatory declaration

Assessing Entity: Gaia-X AISBL or mandated entity

Assessment Process: self-assessment

### Criterion P6.1.5

**objectives**: Before using a `Data Product`, the `Data Consumer` negotiate and co-sign a `Data Product Usage Contract` (DPUC) with the `Data Provider`.

_Note_: A `Data Product Usage Contract` is a Ricardian contract: a contract at law that is both human-readable and machine-readable, cryptographically signed and rendered tamper-proof. 

Basic Conformity: mandatory declaration

Assessing Entity: Gaia-X AISBL or mandated entity

Assessment Process: self-assessment

### Criterion P6.1.6

**objectives**: For each licensed data included in the `Data Product`, the `Data Product Usage Contract` must include an explicit `Data Usage Agreement` signed by the corresponding `Data Licensor` (in case of data liable to GDPR, the signed Data Usage Consent must contain all information required by the regulation).

_Note_: a `Data Licensor` is a natural or legal `Participant` who own usage rights for some `Data`. It can be a data subject as per GDPR for personal data or a primary owner of non-personal data (i.e. not liable to GDPR).

Basic Conformity: mandatory declaration

Assessing Entity: Gaia-X AISBL or mandated entity

Assessment Process: self-assessment

