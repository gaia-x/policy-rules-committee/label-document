# Gaia-X Policy Rules and Labelling Document

## Executive Summary
The Gaia-X Policy Rules define high-level objectives safeguarding the added value and principles of the Gaia-X ecosystem. To allow for validation, the high-level objectives are underpinned by the Gaia-X Labelling Criteria and the Gaia-X Trust Framework.

The intent of the policy rules is to identify clear controls to demonstrate the core European values of Gaia-X: openness, transparency, data protection, security, and portability. Basic conformity defines the minimal set of requirements to be able to participate in a Gaia-X conformant ecosystem. The optional Label levels define additional criteria and additional conformance ensuring measures such as certificates, to achieve additional levels of assurance and trust, with focus on European values and based on EU/EEA legislation. These initial Labels can be extended, and additional Labels can be added in the future, to accommodate for sectorial or geographical needs. Compliance with these policy rule objectives can be achieved via compliance with established standards, certifications, and codes of conduct. 

At this stage the document lists the normative high-level objectives for cloud service providers in the following categories: contractual framework, data protection, cybersecurity and European control. And the document includes a work-in-progress on the requirements towards data exchange services.

The deployment of these high-level objectives can be realized in various levels of conformance. The Basic Conformity level includes the set of rules that define the minimum baseline to be part of the Gaia-X Ecosystem. Those rules ensure a common governance and the basic levels of interoperability across individual ecosystems while letting the users in full control of their choices. In other words, the Gaia-X Ecosystem is the virtual set of participants and Service Offerings following the Gaia-X Basic Conformity requirements. The Trust Framework uses verifiable credentials and linked data representation to build a FAIR knowledge graph of verifiable claims from which additional trust and composability indexes can be automatically computed.

The Labelling Framework extends upon the Basic Conformity level and makes use of verifiable credentials to extend the framework. Thus, it is ensured that all information required to make a qualified choice between different services is available in a consistent and standardized machine-readable form. The Labelling Framework itself is further detailed and translated into concrete criteria and measures in this document. The criteria list brings together the policies and requirements from the various Gaia-X committees – Policies and Rules Committee, Technical Committee, Data Spaces and Business Committee – along with comprehensive assessments to ensure that these requirements can be met. It allows for further differentiation between services that is necessary for users wanting to find services for different purposes and with different needs. It defines minimum qualification levels for the attributes described in the transparency framework.

The Gaia-X Labelling Framework is designed using a set of core principles, starting from the high level objectives which are refined by the labelling criteria. The labels require: consistency among the Gaia-X ecosystem, scalability and extensibility, composability and modularity mapping, referencing of existing standards, and self-assessment and Conformity Assessment Bodies (CAB). 

Gaia-X distinguishes 3 levels of Labels, starting from Label Level 1 (the lowest), up to Label Level 3 (the highest), which represent different degrees of compliance with regard to the goals of transparency, autonomy, data protection, security, interoperability, flexibility, and European Control.

## Preface
The **Policy Rules** define high-level objectives safeguarding the added value and principles of the Gaia-X ecosystem. To allow for validation, the high-level objectives are underpinned by the Gaia-X Labelling Criteria (part of this document) and the [Gaia-X Trust Framework (a separate specification)](https://docs.gaia-x.eu/policy-rules-committee/trust-framework/latest/).

The intent of the policy rules is to identify clear controls to demonstrate the core values of Gaia-X: openness, transparency, data protection, security, and portability. Basic conformity defines the minimal set of requirements to be able to participate in a Gaia-X conformant ecosystem. The optional Label levels define additional criteria and additional conformance ensuring measures such as certificates, to achieve additional levels of assurance and trust, with focus on European values and based on EU/EEA legislation. These initial Labels can be extended, and additional Labels can be added in the future, to accommodate for sectorial or geographical needs.

Please note that, in general, full adherence to applicable local legislation (e.g., in areas such as data protection and security) is a prerequisite and thus not waived or affected by the following policies and rules.

It is worth pointing out that participation within Gaia-X and providing compliant services under the Gaia-X framework shall not prevent any Provider to also provide non-Gaia-X Service Offerings outside the Gaia-X ecosystem.

Compliance with these policy rules objectives can be achieved via compliance with established standards, certifications, and codes of conduct. The addition and maintenance of these accepted standards will be defined as part of the labelling criteria (in this document) and in [the Trust Framework document](https://docs.gaia-x.eu/policy-rules-committee/trust-framework/latest/). Where such tools are not available or approved to demonstrate such compliance, specific methodologies can be further developed and agreed within Gaia-X to be included in the attestation of Service Offerings.

For these high-level objectives, especially the ones related to cybersecurity, we follow, when it is possible,  the current discussions on the European cybersecurity certification scheme for cloud services (the EU cloud services scheme or EUCS). We may also add or substract some high level objectives. When the EUCS is finalised, Gaia-X may consider adapting the objectives in this document.

The Association will update this document on a regular basis.

The **Trust Framework** provides the technical specifications to participate in a Gaia-X Ecosystem, based on the basic conformity requirements defined in the current document. In technical terms, a Gaia-X Ecosystem is a virtual set of Participants and Service Offerings implementing the Gaia-X specifications from the [Gaia-X Trust Framework](https://docs.gaia-x.eu/policy-rules-committee/trust-framework/latest/).

The Trust Framework uses verifiable credentials and linked data representation to build a FAIR knowledge graph of verifiable claims from which additional trust and composability indexes can be automatically computed. 

The **Labelling Framework** is implemented using Verifiable Credentials. Thus, it is ensured that all information required to make a qualified choice between different services is available in a consistent and standardized machine-readable form. This Trust Framework is introduced in the [Gaia-X Architecture Document](https://docs.gaia-x.eu/technical-committee/architecture-document/latest/), section 4.2. 

The Labelling Framework itself is further detailed and translated into concrete criteria and measures in this document. The criteria list brings together the policies and requirements from the various Gaia-X committees – Policies and Rules Committee, Technical Committee, Data Spaces and Business Committee – along with comprehensive assessments to ensure that these requirements can be met. It allows for further differentiation between services that is necessary for users wanting to find services for different purposes and with different needs. It defines minimum qualification levels for the attributes described in the transparency framework.

This document is a works in progress, i.e. it will be further worked on to evolve towards a fully clear and complete specification of the policies, rules and labels. At this stage, it can be clarified that :
- Some of the rules are high-level objectives and still need to be more detailed and specified to be implementable and assessable. The Policy Rules Committee of Gaia-X with its three working groups will continue to work on this in further versions.
- Redundancies are acknowledged. They shall be resolved to the extent possible in the future iterations. Some redundancies are a result of externalities, such as underlying standards, schemes, laws which cannot be resolved.
- Some of the label criteria can be further detailed with the relevant acceptable standards. There will be a process to identify additional standards and to manage the lifecycle of already listed standards, which will follow good practices, using objective criteria. This shall ensure both quality of accepted standards and neutral and fair access to the users of the Labelling Framework.

## Design Principles for Labels

The Gaia-X Labelling Framework is designed using a set of core principles, starting from the high level objectives which are refined by the labelling criteria.

### Consistency among the Gaia-X ecosystem

Gaia-X Labels reflect the essence of our objectives and concepts. They represent the results of decisions and deliverables introduced by the various Gaia-X committees and approved by the Board of Directors. The labelling criteria are always in line with the corresponding concepts and specifications as defined by the Association. 

This version is synchronized explicitly with:
- [Gaia-X Architecture document – TAD2204](https://docs.gaia-x.eu/technical-committee/architecture-document/22.04/)  
- [Gaia-X Trust Framework document - TF2204](https://docs.gaia-x.eu/policy-rules-committee/trust-framework/22.04/)  

### Scalability and extensibility

Based on the three basic labels further Gaia-X Labels can be created to fit new needs, in particular using extension profiles for country and domain specific requirements. Extension profiles can also leverage the labelling criteria by adding and defining on-top requirements for particular purposes. To ensure impact and consistency of Gaia-X Labels, new labels and extensions have to be authorized by the Gaia-X Association (Board of Directors).

### Composability and modularity

Gaia-X Labels are logical groupings of composable service attributes. This results in particular in the assignment of a common set of policies, technical requirements and data spaces criteria to one or multiple of three levels.
At the same time, Gaia-X labels base upon existing schemes, certifications, testates and approved codes of conduct where possible to allow reuse of established standards and thereby simplifying the process. Only in areas where no standard has been identified Gaia-X will introduce its own set of attributes and processes to verify the information given.

### Standards, self-assessment and Conformity Assessment Bodies (CAB)

Gaia-X labels do not normatively reference external documents which are not yet approved (example the current proposal of the Data Act or the EUCS) . Whenever such external documents are approved, Gaia-X may consider adapting its labels in accordance with them.

Conformity with label criteria can be declared by self-assessment (declaration) or supported by external Conformity Assessment Bodies (CAB) (certification) as defined later in this document.

Gaia-X Service Offerings are defined by Provider generated attestations which include claims of adherence to the Labelling Criteria. The proof of a validation of a claim will be technically realized through Verifiable Credentials. The Verifiable Credential can either be issued by a Provider or a CAB directly or it can be created by a trusted Verifiable Credential issuer based on existing documentation (like a signed PDF or paper document).

The Verifiable Credential includes the entity asserting validity of the claim; the list of trusted Verifiable Credentials issuers is maintained in the Gaia-X registry.

Users at any time can query the Attestation of the Service Offering and for each claim extract the entity and the result of the assessment.

The process including the possible process of revoking trust to specific CAB or revocation of validity of attestations is described in the [Gaia-X Trust Framework](https://docs.gaia-x.eu/policy-rules-committee/trust-framework/latest/).

Conformity Assessment Bodies (CAB): The Gaia-X associations reserves its right to choose its own CAB of its own three basic labels. A new detailed document will be issued on the process to choose the relevant CAB. Where the Labelling Framework lacks reference to accepted standards, Gaia-X will define a dedicated Assessment Process including a process to appoint adequate CABs (Conformity Assessment Body). Both processes will follow international recognized good practices, including impartiality, comparability, reliability and accessibility.

### Mapping and Referencing of existing standards
It is intended that this document will provide for each criterion a detailed mapping and referencing to existing standards and certification schemes. This mapping and referencing shall be as detailed as possible, saying, instead of a generic identification of a standard, the relevant sections in such standards shall be identified. 

**Point Of Reference Standard (PORS)**: This document may provide so-called "Point of Reference Standards", short "PORS". PORS shall provide a first impression on existing documents, i.e., standards, conformity assessment programmes, authorities' guidelines, procurement guidelines, etc. Indicated PORS are neither a guarantee that Gaia-X criteria are fully met, nor that compliance with respectively implementation of such PORS will be required to meet a Gaia-X criterion. It is rather a point of reference to support identifying related processes. 
_Note: PORS will be added with minimum review. Once there is a minor relation this may suffice to add such standards as PORS. It is expected to review such standards in future iterations to upgrade such references to any more sophisticate type of reference. Likewise, a further review may result in a deletion of the reference, if the relations is considered too weak._

**Example Standard**: This document may provide so-called "Example Standards". Example Standards shall identify potential means of implementation. Gaia-X strives to refer to existing standards and controls to the extent possible. Re-drafting shall be prevented. Nonetheless, Gaia-X and referenced standards may have a different focus and high level objective. Example standards shall provide for possibilities how criteria may be implemented. Implementation as provided by such standards is not mandatory. Nor it is required to comply with any such standards. Gaia-X will provide additional notes, if significant differences will be identified. Example Standards shall especially help in evaluating conformity with Gaia-X, as Example Standards can be considered "implementation guidance". 
_Note: Example Standards will be added after following a thorough assessment by the Gaia-X Working Groups maintaining this document. Such assessment shall follow a transparent process. No Example Standards shall be listed, prior such process is defined and applied in the determination. The process shall foresee that third-party standards may reach out to Gaia-X and suggest being enlisted._

**Permissible Standard**: This document may provide so-called "Permissible Standards". Permissible Standards shall identify standards respectively requirements / controls within such standards, where implementation shall be considered a prima facie evidence of conformity with the related Gaia-X criterion.
_Note: Permissible Standards can only be added following a thorough assessment by the Gaia-X Working Groups maintaining this document. Such assessment shall follow a transparent process. No Permissible Standards shall be listed, prior such process is defined and applied in the determination. The process shall foresee that third-party standards may reach out to Gaia-X and suggest being enlisted. The process shall cover both, the material requirements as well as the overarching conformity assessment programme, i.e., the means how such Permissible Standard determines whether the subject of such assessment is indeed conform / compliant._

## Proof of Concept / Bootstrapping
As of today, Gaia-X has identified "Acceptable Standards". To allow Gaia-X to become operational by means of a proof of concept or by means of an early adoption phase (pilot), soon, Accepted Standards shall be considered prima facie evidence of sufficient implementation of Gaia-X criteria. 
_Note: If not stated differently in this document, implementation of such Accepted Standard shall suffice. It shall not be required to underpin implementation of individual requirements / controls of the respective Accepted Standard._
_Note: Statements of Conformity relating to such Acceptable Standards shall be considered a Trusted Source. To the extent the applicable conformity assessment bodies will provide the Statement of Conformity as a Verifiable Credential in accordance with the Gaia-X specifications, those shall also be considered Trust Anchors._

### Conformity Assessment Programme and Assessibility
The Gaia-X Policy Rules and Labelling Criteria must become and remain assessible at all times. Gaia-X is currently developing accompanying documents outlining the overarching conformity assessment programme and process. 

Also this PRLD will further evolve to enhance the assessibility of its criteria to the extent necessary, e.g. where Gaia-X will not or can not rely on existing standards and conformity assessment programmes.

Gaia-X anticipates that the requirements outlined in this document are assessible. If comparability of assessment results can not be guaranteed, or if ambiguities exist, Gaia-X may have to adapt these rules, criteria or assessment mechanisms in future versions.

In this vein and as mentioned elsewhere in this document, Gaia-X will monitor current regulatory developments as well as developments in the field of standards and conformity assessment programmes. Whilst Gaia-X may consider existing drafts as inspiration, Gaia-X does not endorse any such drafts. Likewise, Gaia-X remains in control of whether to adapt its requirements to future iterations of any such external developments. 

### Federation of Verification

Gaia-X labels are issued according to determined criteria and assessments in a federated manner. The concept of modularity also allows Gaia-X to reuse existing certifications for the underlying service attributes whenever possible, hence reducing the cost and complexity of embracing Gaia-X labelling, especially for existing, already certified, services. Assessment Processes defined by Gaia-X itself will also base on a federation of responsibilities.

### Further design principles

The modularity concept requires Gaia-X labelling criteria to describe rather high-level objectives as the detailed requirements are further described in the corresponding standards that are acknowledged. 
As of today, Gaia-X Labels are issued to a specific Service Offering unless stated otherwise. Only the criteria defined by the DSBC apply to data-sharing networks and define the governance, usage policies and obligations among ecosystem partners.

## Gaia-X Policy Rules and Labelling Criteria for Providers
Note: we use the term **'Provider'** throughout this section as the short denominator for a cloud Service Provider or CSP, i.e., the participant who provides cloud Service Offerings in the Gaia-X ecosystem. We use the term 'Customer' in this section to denominate the cloud service Customer, i.e., the participant who consumes a Service Offering from a cloud Service Provider.

Note: we use the term **'Customer Data'** throughout this section for all customer provided or generated data, both personal and non-personal data, as processed by a Provider. This is not about the *data-about-the-Customer*, which the Provider needs to administer the service offering, to deploy, meter and bill the service to the Customer. Such *data-about-the-Customer* or *know-my-customer-data* needs to be handled according to applicable legislation by the Provider and this falls outside the scope of this PRLD. Please note that additional contractual arrangements, inclusions or exclusions, can be made regarding specific types of data in scope of a service agreement.

Note: whereas certain rules have originated from personal data privacy legislation, and other rules are suggested in a non-personal context, in practice, it is in most cases impossible for a Provider to differentiate between these data types. The Provider does not, and in many cases ought not even know which type of data is stored or processed with its services. Still, we have explicitly indicated which rules apply to personal data, where relevant.

Note: the policy rules and label criteria are listed using a hierarchical numbering system, prefixed by a "P" to indicated Provider targeted criteria. The hierarchical numbering allows to assign stable numbers to the criteria, also when future additions or deletions are made.

Note: we use the following abbreviations in this section: BC (basic conformity), L1 (label level 1), L2 (label level 2) and L3 (label level 3).

### Contractual framework
This section reflects provisions associated to the contractual framework between a 'Provider' and a 'Customer', required for any Service Offering regardless of its type, purpose, or processed category of data. It divides in requirements related to the governance of contract and material aspects that shall be addressed in contracts. This section, and subordinate criteria shall not provide exact and exhaustive contractual language. It shall rather allow providers to reflect the requirements subject to their individual needs of structure and language.

Additionally, it is not expected that individual contracts will be subject to an evaluation process by Gaia-X. Gaia-X will rather focus on evaluating a process, reflected by documented internal policies or procedures, that safeguard conformity with the requirements laid out in this section.

#### Contractual governance

**Criterion P1.1.1 : The Provider shall offer the ability to establish a legally binding act. This legally binding act shall be documented.**

Note: The Provider needs to ensure a process that guarantees that a legally binding act is in place before delivering any form of service.

Note: The legally binding act can be a contract.

Note: Documented can be by any means, provided that both parties have the same access to such documentation, including the possibility to technically copy and share such documentation without hindrance. The possibility to technically copy and share without hindrance does not prevent the parties to agree upon any NDA or other means, that might provide for reasonable legal limitations.

BC, L1, L2, L3: mandatory declaration

Assessing Entity: Gaia-X AISBL or mandated entity

Assessment Process: self-assessment

**Criterion P1.1.2 : The Provider shall have an option for each legally binding act to be governed by EU/EEA/Member State law.**

BC: this criterion is not applicable

L1, L2, L3: mandatory declaration

Assessing Entity: Gaia-X AISBL or mandated entity

Assessment Process: self-assessment

Point Of Reference Standards:
- SecNumCloud 3.2.a – 19.1.c


**Criterion P1.1.3 : The Provider shall clearly identify for which parties the legal act is binding.**

BC, L1, L2, L3: mandatory declaration

Assessing Entity: Gaia-X AISBL or mandated entity

Assessment Process: self-assessment

**Criterion P1.1.4 : The Provider shall ensure that the legally binding act covers the entire provision of the Service Offering**

Rationale: The provisions of the Service Offering may comprise of several elements. Increased complexities of individual Service Offerings must not undermine the necessity of a documented legally binding act. To address practical needs, the legally binding act may comprise of multiple separate documents, e.g., a master agreement and exhibits such as service level agreements or data protection agreement.

BC, L1, L2, L3: mandatory declaration

Assessing Entity: Gaia-X AISBL or mandated entity

Assessment Process: self-assessment

Point Of Reference Standards:
- CISPE-DATA PROTECTION CODE OF CONDUCT FOR CLOUD INFRASTRUCTURE SERVICE PROVIDERS-VERSION 9TH FEBRUARY 2021 – Requirements 4.2 Contractual terms and conditions of the CISP’s services
- EU Cloud CoC - 5.1.A, 5.1.H


**Criterion P1.1.5 : The Provider shall clearly identify in each legally binding act the applicable governing law.**

BC, L1, L2, L3: mandatory declaration

Assessing Entity: Gaia-X AISBL or mandated entity

Assessment Process: self-assessment

Point Of Reference Standards:
- SecNumCloud 3.2.a – 19.1.c

#### General material requirements and transparency

**Criterion P1.2.1 : The Provider shall ensure there are specific provisions regarding service interruptions and business continuity (e.g., by means of a service level agreement), Provider's bankruptcy or any other reason by which the Provider may cease to exist in law.**

BC, L1, L2, L3: mandatory declaration

Assessing Entity: Gaia-X AISBL or mandated entity

Assessment Process: self-assessment

Point Of Reference Standards:
- SecNumCloud 3.2.a – 19.1.j


**Criterion P1.2.2 : The Provider shall ensure there are provisions governing the rights of the parties to use the service and any Customer Data therein.**

BC, L1, L2, L3: mandatory declaration

Assessing Entity: Gaia-X AISBL or mandated entity

Assessment Process: self-assessment

Point Of Reference Standards:
- ISO19944
- SecNumCloud 3.2.a - 19.1 

**Criterion P1.2.3 : The Provider shall ensure there are provisions governing changes, regardless of their kind.**

BC, L1, L2, L3: mandatory declaration

Assessing Entity: Gaia-X AISBL or mandated entity

Assessment Process: self-assessment

Point Of Reference Standards:
- SecNumCloud 12.2a; 14.2a; 15.4.a


**Criterion P1.2.4 : The Provider shall ensure there are provisions governing aspects regarding copyright or any other intellectual property rights.**

BC, L1, L2, L3: mandatory declaration

Assessing Entity: Gaia-X AISBL or mandated entity

Assessment Process: self-assessment

Point Of Reference Standards:
- SecNumCloud 3.2.a – 7.2.c
- SWIPO-IAAS-CODE-OF-CONDUCT-VERSION-2020-27-MAY-2020-V3.0 – SCR02 – Requirement SCR02

**Criterion P1.2.5 : The Provider shall declare the general location of any processing of Customer Data, allowing the Customer to determine the applicable jurisdiction and to comply with Customer's requirements in the context of its business and operational context.**


Note: 
* The general location is a geographical reference, such as city or city region area.
* Business and operational context shall address elements such as business continuity, by e.g., safeguarding minimum distances between Customer's processing activities.

BC, L1, L2, L3: mandatory declaration

Assessing Entity: Gaia-X AISBL or mandated entity

Assessment Process: self-assessment

Point Of Reference Standards: 
- SecNumCloud 3.2.a – 19.1.b
- CISPE-DATA PROTECTION CODE OF CONDUCT FOR CLOUD INFRASTRUCTURE SERVICE PROVIDERS-VERSION 9TH FEBRUARY 2021 – Requirements 4.4 
- EU Cloud CoC; Section 5.3 and 5.4, more specifically Control 5.3.E, 5.3.G, 5.4.B


**Criterion P1.2.6 : The Provider shall explain how information about subcontractors and related Customer Data localization will be communicated.**

Note: this applies to the subcontractors essential to the provision of the Service Offering, including any sub-processors.

BC, L1, L2, L3: mandatory declaration

Assessing Entity: Gaia-X AISBL or mandated entity

Assessment Process: self-assessment

Point Of Reference Standards:
- SecNumCloud 3.2.a – 15.1; 19.1.b; 19.2.a

**Criterion P1.2.7 : The Provider shall communicate to the Customer where the applicable jurisdiction(s) of subcontractors will be.**

Note: this applies to the subcontractors essential to the provision of the Service Offering, including any sub-processors

BC, L1, L2, L3: mandatory declaration

Assessing Entity: Gaia-X AISBL or mandated entity

Assessment Process: self-assessment

Point Of Reference Standards: 
- SecNumCloud 3.2.a - 19.6.c
- CISPE-DATA PROTECTION CODE OF CONDUCT FOR CLOUD INFRASTRUCTURE SERVICE PROVIDERS-VERSION 9TH FEBRUARY 2021 – Requirements 4.5 Sub-processing
- EU Cloud CoC; Section 5.3, more specifically Control 5.3.E, 5.3.G

**Criterion P1.2.8 : The Provider shall include in the contract the contact details where Customer may address any queries regarding the Service Offering and the contract.**

Note: Queries include request during the pre-contractual state, before coming to an agreement.

BC, L1, L2, L3: mandatory declaration

Assessing Entity: Gaia-X AISBL or mandated entity

Assessment Process: self-assessment


**Criterion P1.2.9 : The Provider shall adopt the Gaia-X Trust Framework, by which Customers may verify Provider’s Service Offering.**

Note: The Gaia-X conformity verification is based on the Trust Framework and based on self-descriptions validated by verifiable credentials.

BC, L1, L2, L3: mandatory declaration

Assessing Entity: Gaia-X AISBL or mandated entity

Assessment Process: self-assessment



Note: transparency can be created via a link to an environmental impact report of the Provider. This may be an aggregate statement on a portfolio of services, not necesessarily the impact of an individual Service Offering. The report shall describe the consumption of natural resources such as water and energy sources, the carbon footprint, the use of polluants and other factors.

BC, L1, L2, L3: mandatory declaration

Assessing Entity: Gaia-X AISBL or mandated entity

Assessment Process: self-assessment

**Criterion P1.2.11 : The Provider shall ensure that the Service Offering meets or relies on an infrastructure Services Offering which meets a high standard in energy efficiency, meeting an annual target of PUE of 1.3 in cool climates and 1.4 in warm climates**

Note: By January 1, 2025 the metric should be met by any new data centre at full capacity used to provide the Service Offering. Pre-existing data centres will achieve these same targets by January 1, 2030. The targets apply to all data centres larger than 50KW of maximum IT power demand.

Assessing Entity: 

- L1: Gaia-X AISBL or mandated entity

- L2/L3: Accredited Conformity Assessment Body for one of the Accepted Standards

Assessment Process: 

- L1: legally binding statement towards Gaia-X via the Trust Framework to comply with the Gaia-X Labelling criterion or public registered adherence to one of the Accepted Standards.

- L2 / L3: Assessment process by an accredited Conformity Assessment Body of one of the Accepted Standards

Accepted Standards: Climate Neutral Data Centre Pact (CNDCP)


**Criterion P1.2.12 : The Provider shall ensure that the Service Offering meets or relies on an infrastructure Services Offering for which electricity demand will be matched by 75% renewable energy or hourly carbon-free energy by 31st December 2025, and 100% by 31st December 2030.**

Assessing Entity: 

- L1: Gaia-X AISBL or mandated entity

- L2/L3: Accredited Conformity Assessment Body for one of the Accepted Standards

Assessment Process: 

- L1: legally binding statement towards Gaia-X via the Trust Framework to comply with the Gaia-X Labelling criterion or public registered adherence to one of the Accepted Standards.

- L2 / L3: Assessment process by an accredited Conformity Assessment Body of one of the Accepted Standards

Accepted Standards: Climate Neutral Data Centre Pact (CNDCP)


**Criterion P1.2.13 : The Provider shall ensure that the Service Offering meets or relies on an infrastructure Services Offering that will meet a high standard for water conservation demonstrated though the application through the application of a location and source sensitive water usage effectiveness (WUE)target of 0.4 L/kWh in areas with water stress**

Note: By January 1, 2025 new data centres at full capacity in cool climates that use potable water will be designed to meet a maximum WUE of 0.4 L/kWh in areas with water stress.The limit for WUE can be modified based on climate, stress and water type to encourage the use of sustainable water sources for cooling.By December 31, 2040, existing data centres that replace a cooling system will meet the WUE target applied to new data centres.

Assessing Entity: 

- L1: Gaia-X AISBL or mandated entity

- L2/L3: Accredited Conformity Assessment Body for one of the Accepted Standards

Assessment Process: 

- L1: legally binding statement towards Gaia-X via the Trust Framework to comply with the Gaia-X Labelling criterion or public registered adherence to one of the Accepted Standards.

- L2 / L3: Assessment process by an accredited Conformity Assessment Body of one of the Accepted Standards

Accepted Standards: Climate Neutral Data Centre Pact (CNDCP)

#### Technical compliance requirements

**Criterion P1.3.1 : Service Offering shall include a policy using a common Domain-Specific Language (DSL) to describe Permissions, Requirements and Constraints.**

Source: AD v2112, Chapter: 4.1

Applicable for BC, L1, L2, L3

Assessing Entity: Gaia-X Compliance Service Provider

Assessment Process: Gaia-X Trust Framework checking the attestation

**Criterion P1.3.2 : Service Offering requires being operated by Service Offering Provider with a verified identity.**

Source: AD v2112, Chapter: 4.2 / 4.3

Applicable for BC, L1, L2, L3

Assessing Entity: Gaia-X Compliance Service Provider

Assessment Process: Gaia-X Trust Framework checking the attestation

**Criterion P1.3.3 : Service Offering must provide a conformant self-description.**

Source: AD v2112, Chapter: 4.4 & 4.6.2

Applicable for BC, L1, L2, L3

Assessing Entity: Gaia-X Compliance Service Provider

Assessment Process: Gaia-X Trust Framework checking the attestation

**Criterion P1.3.4 : Self-Description attributes need to be consistent across linked Self-Descriptions.**

Source: AD v2112, Chapter: 4.4 & 4.6.2

Applicable for BC, L1, L2, L3

Assessing Entity: Gaia-X Compliance Service Provider

Assessment Process: Gaia-X Trust Framework checking the self-description


**Criterion 1.3.5 : The Provider shall ensure that the Consumer uses a verified identity provided by the Federator.**

Source: AD v2112, Chapter: 4.4.1

Applicable for BC, L1, L2, L3

Assessing Entity: Gaia-X Compliance Service Provider

Assessment Process: Gaia-X Trust Framework checking the attestation

### Data Protection

This section only applies in case of processing personal Customer Data. It reflects GDPR requirements without extending GDPR's obligations, and it cites some of these requirements as they are judged to be explicitly relevant. By principle, this section shall only apply to personal data that are processed and are subject to the commercial relationship between the Customer and the Provider (we call them *'personal Customer Data'*), but not those personal data that are processed by the Provider to establish and maintain such commercial relationship for its own purposes, e.g., contract handling and invoicing. Provided a service offering will not process any personal data in this sense, requirements as laid down in this section shall not apply.

#### General

**Criterion P2.1.1 : The Provider shall offer the ability to establish a contract under Union or EU/EEA/Member State law and specifically addressing GDPR requirements.**

Note: GDPR requires EU/EEA or Member State law to be applicable. The Provider needs to ensure a process that guarantees that a legally binding act is in place before delivering any form of service

Note: The GDPR requires suitable documentation, whilst clarifying, e.g., in Art. 28 (9) GDPR, that such documentation shall be in writing, including electronic form

Assessing Entity: L1: Gaia-X AISBL or mandated entity

BC: not applicable

L1: mandatory declaration

L2, L3: mandatory certification

L2/L3: In cases of a Code of Conduct (Art. 40 GDPR): accredited monitoring body for the respective Code of Conduct, Art. 41 GDPR; In case of a Certification (Art. 42 GDPR): accredited Certification Body for the respective Certification (Art. 43 GDPR) 

Assessment Process: L1: legally binding statement towards Gaia-X via the Trust Framework to comply with the Gaia-X Labelling criterion. 

L2 / L3: In case of a Code of Conduct (Art. 40 GDPR): assessment process as defined by the respective Code of Conduct / accredited monitoring body; In case of a Certification (Art. 43 GDPR): assessment process as defined by the respective Certification / accredited certification body.  

Accepted Standards: Codes of Conduct acc. Art. 40 GDPR (currently CISPE, EU Cloud CoC) or certifications acc. Art. 42 GDPR


**Criterion P2.1.2 : The Provider shall define the roles and responsibilities of each party.**

Note: This considers the roles and responsibilities of the parties involved in the scope of this Service Offering.

BC and L1: mandatory declaration

L2, L3: mandatory certification

Assessing Entity: L1: Gaia-X AISBL or mandated entity

L2/L3: In cases of a Code of Conduct (Art. 40 GDPR): accredited monitoring body for the respective Code of Conduct, Art. 41 GDPR; In case of a Certification (Art. 42 GDPR): accredited Certification Body for the respective Certification (Art. 43 GDPR) 

Assessment Process: L1: legally binding statement towards Gaia-X via the Trust Framework to comply with the Gaia-X Labelling criterion. 

L2 / L3: In case of a Code of Conduct (Art. 40 GDPR): assessment process as defined by the respective Code of Conduct / accredited monitoring body; In case of a Certification (Art. 43 GDPR): assessment process as defined by the respective Certification / accredited certification body.  

Accepted Standards: Codes of Conduct acc. Art. 40 GDPR (currently CISPE, EU Cloud CoC) or certifications acc. Art. 42 GDPR


**Criterion P2.1.3 : The Provider shall clearly define the technical and organizational measures in accordance with the roles and responsibilities of the parties, including an adequate level of detail.**

BC and L1: mandatory declaration

L2, L3: mandatory certification

Assessing Entity: L1: Gaia-X AISBL or mandated entity 

L2/L3: In cases of a Code of Conduct (Art. 40 GDPR): accredited monitoring body for the respective Code of Conduct, Art. 41 GDPR; In case of a Certification (Art. 42 GDPR): accredited Certification Body for the respective Certification (Art. 43 GDPR) 

Assessment Process: L1: legally binding statement towards Gaia-X via the Trust Framework to comply with the Gaia-X Labelling criterion. 

L2 / L3: In case of a Code of Conduct (Art. 40 GDPR): assessment process as defined by the respective Code of Conduct / accredited monitoring body; In case of a Certification (Art. 43 GDPR): assessment process as defined by the respective Certification / accredited certification body.  

Accepted Standards: Codes of Conduct acc. Art. 40 GDPR (currently CISPE, EU Cloud CoC) or certifications acc. Art. 42 GDPR

#### GDPR Art. 28

**Criterion P2.2.1 : The Provider shall be ultimately bound to instructions of the Customer.**

BC and L1: mandatory declaration

L2, L3: mandatory certification

Assessing Entity: L1: Gaia-X AISBL or mandated entity 
L2/L3: In cases of a Code of Conduct (Art. 40 GDPR): accredited monitoring body for the respective Code of Conduct, Art. 41 GDPR; In case of a Certification (Art. 42 GDPR): accredited Certification Body for the respective Certification (Art. 43 GDPR) 

Assessment Process: L1: legally binding statement towards Gaia-X via the Trust Framework to comply with the Gaia-X Labelling criterion. 

L2 / L3: In case of a Code of Conduct (Art. 40 GDPR): assessment process as defined by the respective Code of Conduct / accredited monitoring body; In case of a Certification (Art. 43 GDPR): assessment process as defined by the respective Certification / accredited certification body.  

Accepted Standards: Codes of Conduct acc. Art. 40 GDPR (currently CISPE, EU Cloud CoC) or certifications acc. Art. 42 GDPR


**Criterion P2.2.2 : The Provider shall clearly define how Customer may instruct, including by electronic means such as configuration tools or APIs.**

BC and L1: mandatory declaration

L2, L3: mandatory certification

Assessing Entity: L1: Gaia-X AISBL or mandated entity 

L2/L3: In cases of a Code of Conduct (Art. 40 GDPR): accredited monitoring body for the respective Code of Conduct, Art. 41 GDPR; In case of a Certification (Art. 42 GDPR): accredited Certification Body for the respective Certification (Art. 43 GDPR) 

Assessment Process: L1: legally binding statement towards Gaia-X via the Trust Framework to comply with the Gaia-X Labelling criterion.  

L2 / L3: In case of a Code of Conduct (Art. 40 GDPR): assessment process as defined by the respective Code of Conduct / accredited monitoring body; In case of a Certification (Art. 43 GDPR): assessment process as defined by the respective Certification / accredited certification body.  

Accepted Standards: Codes of Conduct acc. Art. 40 GDPR (currently CISPE, EU Cloud CoC) or certifications acc. Art. 42 GDPR


**Criterion P2.2.3 : The Provider shall clearly define if and to which extent third country transfer will take place.**

BC and L1: mandatory declaration

L2: mandatory certification

L3: not applicable

Assessing Entity:  L1: Gaia-X AISBL or mandated entity 

L2: In cases of a Code of Conduct (Art. 40 GDPR): accredited monitoring body for the respective Code of Conduct, Art. 41 GDPR; In case of a Certification (Art. 42 GDPR): accredited Certification Body for the respective Certification (Art. 43 GDPR) 

Assessment Process: L1: legally binding statement towards Gaia-X via the Trust Framework to comply with the Gaia-X Labelling criterion.  

L2 In case of a Code of Conduct (Art. 40 GDPR): assessment process as defined by the respective Code of Conduct / accredited monitoring body; In case of a Certification (Art. 43 GDPR): assessment process as defined by the respective Certification / accredited certification body.  

Accepted Standards: Codes of Conduct acc. Art. 40 GDPR (currently CISPE, EU Cloud CoC) or certifications acc. Art. 42 GDPR

This rule is not applicable to Label Level 3.


**Criterion P2.2.4 : The Provider shall clearly define if and to the extent third country transfers will take place, and by which means of Chapter V GDPR these transfers will be protected.**

BC: not applicable

L1: mandatory declaration

L2, L3: mandatory certification

Assessing Entity:

L1: Gaia-X AISBL or mandated entity 

L2: In cases of a Code of Conduct (Art. 40 GDPR): accredited monitoring body for the respective Code of Conduct, Art. 41 GDPR; In case of a Certification (Art. 42 GDPR): accredited Certification Body for the respective Certification (Art. 43 GDPR) 

Assessment Process: 

L1: legally binding statement towards Gaia-X via the Trust Framework to comply with the Gaia-X Labelling criterion.  

L2 : In case of a Code of Conduct (Art. 40 GDPR): assessment process as defined by the respective Code of Conduct / accredited monitoring body; In case of a Certification (Art. 43 GDPR): assessment process as defined by the respective Certification / accredited certification body.  

Accepted Standards: Codes of Conduct acc. Art. 40 GDPR (currently CISPE, EU Cloud CoC) or certifications acc. Art. 42 GDPR

This rule is not applicable for Label Level 3.


**Criterion P2.2.5 : The Provider shall clearly define if and to which extent sub-processors will be involved.**

BC and L1: mandatory declaration

L2, L3: mandatory certification

Assessing Entity: 

L1: Gaia-X AISBL or mandated entity 

L2/L3: In cases of a Code of Conduct (Art. 40 GDPR): accredited monitoring body for the respective Code of Conduct, Art. 41 GDPR; In case of a Certification (Art. 42 GDPR): accredited Certification Body for the respective Certification (Art. 43 GDPR) 

Assessment Process: 

L1: legally binding statement towards Gaia-X via the Trust Framework to comply with the Gaia-X Labelling criterion.  

L2 / L3: In case of a Code of Conduct (Art. 40 GDPR): assessment process as defined by the respective Code of Conduct / accredited monitoring body; In case of a Certification (Art. 43 GDPR): assessment process as defined by the respective Certification / accredited certification body.  

Accepted Standards: Codes of Conduct acc. Art. 40 GDPR (currently CISPE, EU Cloud CoC) or certifications acc. Art. 42 GDPR


**Criterion P2.2.6 : The Provider shall clearly define if and to the extent sub-processors will be involved, and the measures that are in place regarding sub-processors management.**

BC and L1: mandatory declaration

L2, L3: mandatory certification

Assessing Entity: 

L1: Gaia-X AISBL or mandated entity 

L2/L3: In cases of a Code of Conduct (Art. 40 GDPR): accredited monitoring body for the respective Code of Conduct, Art. 41 GDPR; In case of a Certification (Art. 42 GDPR): accredited Certification Body for the respective Certification (Art. 43 GDPR) 

Assessment Process: L1: legally binding statement towards Gaia-X via the Trust Framework to comply with the Gaia-X Labelling criterion.  

L2 / L3: In case of a Code of Conduct (Art. 40 GDPR): assessment process as defined by the respective Code of Conduct / accredited monitoring body; In case of a Certification (Art. 43 GDPR): assessment process as defined by the respective Certification / accredited certification body.  

Accepted Standards: Codes of Conduct acc. Art. 40 GDPR (currently CISPE, EU Cloud CoC) or certifications acc. Art. 42 GDPR


**Criterion P2.2.7 : The Provider shall define the audit rights for the Customer.**

BC and L1: mandatory declaration

L2, L3: mandatory certification

Assessing Entity: 

L1: Gaia-X AISBL or mandated entity 

L2/L3: In cases of a Code of Conduct (Art. 40 GDPR): accredited monitoring body for the respective Code of Conduct, Art. 41 GDPR; In case of a Certification (Art. 42 GDPR): accredited Certification Body for the respective Certification (Art. 43 GDPR) 

Assessment Process: L1: legally binding statement towards Gaia-X via the Trust Framework to comply with the Gaia-X Labelling criterion. 

L2 / L3: In case of a Code of Conduct (Art. 40 GDPR): assessment process as defined by the respective Code of Conduct / accredited monitoring body; In case of a Certification (Art. 43 GDPR): assessment process as defined by the respective Certification / accredited certification body.  

Accepted Standards: Codes of Conduct acc. Art. 40 GDPR (currently CISPE, EU Cloud CoC) or certifications acc. Art. 42 GDPR


#### GDPR Art. 26

**Criterion P2.3.1 : In case of a joint controllership, the Provider shall ensure an arrangement pursuant to Art. 26 (1) GDPR is in place.**

BC: not applicable

L1: mandatory declaration

L2, L3: mandatory certification

Assessing Entity:

L1: Gaia-X AISBL or mandated entity

L2/L3: In cases of a Code of Conduct (Art. 40 GDPR): accredited monitoring body for the respective Code of Conduct, Art. 41 GDPR; In case of a Certification (Art. 42 GDPR): accredited Certification Body for the respective Certification (Art. 43 GDPR) 

Assessment Process: L1: legally binding statement towards Gaia-X via the Trust Framework to comply with the Gaia-X Labelling criterion.  

L2 / L3: In case of a Code of Conduct (Art. 40 GDPR): assessment process as defined by the respective Code of Conduct / accredited monitoring body; In case of a Certification (Art. 43 GDPR): assessment process as defined by the respective Certification / accredited certification body.  

Accepted Standards: Codes of Conduct acc. Art. 40 GDPR (currently CISPE, EU Cloud CoC) or certifications acc. Art. 42 GDPR


**Criterion P2.3.2 : In case of a joint controllership, at a minimum, the Provider shall ensure that the very essence of such agreement is communicated to data subjects.**

BC and L1: mandatory declaration

L2, L3: mandatory certification

Assessing Entity: 

L1: Gaia-X AISBL or mandated entity

L2/L3: In cases of a Code of Conduct (Art. 40 GDPR): accredited monitoring body for the respective Code of Conduct, Art. 41 GDPR; In case of a Certification (Art. 42 GDPR): accredited Certification Body for the respective Certification (Art. 43 GDPR) 

Assessment Process: L1: legally binding statement towards Gaia-X via the Trust Framework to comply with the Gaia-X Labelling criterion. 

L2 / L3: In case of a Code of Conduct (Art. 40 GDPR): assessment process as defined by the respective Code of Conduct / accredited monitoring body; Certification (Art. 42): Inspection/Verification/

Accepted Standards: Codes of Conduct acc. Art. 40 GDPR (currently CISPE, EU Cloud CoC) or certifications acc. Art. 42 GDPR


**Criterion P2.3.3 : In case of a joint controllership, the Provider shall publish a point of contact for data subjects.**

BC and L1: mandatory declaration

L2, L3: mandatory certification

Assessing Entity: 

L1: Gaia-X AISBL or mandated entity 

L2/L3: In cases of a Code of Conduct (Art. 40 GDPR): accredited monitoring body for the respective Code of Conduct, Art. 41 GDPR; In case of a Certification (Art. 42 GDPR): accredited Certification Body for the respective Certification (Art. 43 GDPR) 

Assessment Process: L1: legally binding statement towards Gaia-X via the Trust Framework to comply with the Gaia-X Labelling criterion.  

L2 / L3: In case of a Code of Conduct (Art. 40 GDPR): assessment process as defined by the respective Code of Conduct / accredited monitoring body; In case of a Certification (Art. 43 GDPR): assessment process as defined by the respective Certification / accredited certification body. 

Accepted Standards: Codes of Conduct acc. Art. 40 GDPR (currently CISPE, EU Cloud CoC) or certifications acc. Art. 42 GDPR


### Cybersecurity

Safeguarding the appropriate security of service offerings and processed elements, is a key and state-of-art principle. Therefore, this section applies to any service offering, regardless of its Provider, type, purpose, or processed category of data. It is acknowledged that implementing cybersecurity related measures may apply in most cases to the Provider's organisation, rather than the explicit service offering. However, theoretically, measures may deviate between different service offerings. Thus, where measures will be implemented at an organisation-wide level, their inheritance shall suffice for this section. Where measures will be implemented on a per service offering level, individual evaluation per service offering will be required.

For all the security requirements, the criteria follow as much as possible the current discussions on the European Cloud Scheme (EUCS). When the EUCS is finalized, Gaia-X will adapt consequently these criteria. Therefore the terms on the different criteria on this item should be read in the light of EUCS.

**Criterion P3.1.1 : Organization of information security: Plan, implement, maintain and continuously improve the information security framework within the organisation.**

BC: mandatory declaration

L1: mandatory declaration and external review

L2, L3: mandatory certification


Assessing Entity: 

L1: internal + authorized entity according to the EUCS Level Basic; ad interim: internal+ external confirmation that the internal audit followed recognized standards and/or good practices 

L2: Assessing entity authorized according to the respective standards

L3: Assessing entity authorized according to the respective standards

Assessment Process: 

L1: internal audit; externally confirmed to be following recognized standards and/or good practices 

L2: onsite assessment following assessment process according to the respective standards

L3: According to process for EUCS Level High ; ad interim: see Label Level 2

Accepted Standards: if scope is matching: C5, TISAX, SOC2, SecNumCloud, ISO 27001, CSA


**Criterion P3.1.2 : Information Security Policies: Provide a global information security policy, derived into policies and procedures regarding security requirements and to support business requirements**

BC: mandatory declaration

L1: mandatory declaration and external review

L2, L3: mandatory certification

Assessing Entity: 

L1: internal + authorized entity according to the EUCS Level Basic; ad interim: internal+ external confirmation that the internal audit followed recognized standards and/or good practices 

L2: Assessing entity authorized according to the respective standards

L3: Assessing entity authorized according to the respective standards

Assessment Process: 

L1: internal audit; externally confirmed to be following recognized standards and/or good practices 

L2: onsite assessment following assessment process according to the respective standards

L3: According to process for EUCS Level High ; ad interim: see Label Level 2

Accepted Standards: if scope is matching: C5, TISAX, SOC2, SecNumCloud, ISO 27001, CSA


**Criterion P3.1.3 : Risk Management: Ensure that risks related to information security are properly identified, assessed, and treated, and that the residual risk is acceptable to the CSP.**

BC: mandatory declaration

L1: mandatory declaration and external review

L2, L3: mandatory certification

Assessing Entity: 

L1: internal + authorized entity according to the EUCS Level Basic; ad interim: internal+ external confirmation that the internal audit followed recognized standards and/or good practices 

L2: Assessing entity authorized according to the respective standards

L3: Assessing entity authorized according to the respective standards

Assessment Process: 

L1: internal audit; externally confirmed to be following recognized standards and/or good practices 

L2: onsite assessment following assessment process according to the respective standards

L3: According to process for EUCS Level High ; ad interim: see Level 2

Accepted Standards: if scope is matching: C5, TISAX, SOC2, SecNumCloud, ISO 27001, CSA


**Criterion P3.1.4 : Human Resources: Ensure that employees understand their responsibilities, are aware of their responsibilities with regard to information security, and that the organisation's assets are protected in the event of changes in responsibilities or termination.**

BC: mandatory declaration

L1: mandatory declaration and external review

L2, L3: mandatory certification

Assessing Entity: 

L1: internal + authorized entity according to the EUCS Level Basic; ad interim: internal+ external confirmation that the internal audit followed recognized standards and/or good practices 

L2: Assessing entity authorized according to the respective standards

L3: Assessing entity authorized according to the respective standards

Assessment Process: 

L1: internal audit; externally confirmed to be following recognized standards and/or good practices 

L2: onsite assessment following assessment process according to the respective standards

L3: According to process for EUCS Level High ; ad interim: see Label Level 2

Accepted Standards: if scope is matching: C5, TISAX, SOC2, SecNumCloud, ISO 27001, CSA


**Criterion P3.1.5 : Asset Management: Identify the organisation's own assets and ensure an appropriate level of protection throughout their lifecycle.**

BC: mandatory declaration

L1: mandatory declaration and external review

L2, L3: mandatory certification

Assessing Entity: 

L1: internal + authorized entity according to the EUCS Level Basic; ad interim: internal+ external confirmation that the internal audit followed recognized standards and/or good practices 

L2: Assessing entity authorized according to the respective standards

L3: Assessing entity authorized according to the respective standards

Assessment Process: 

L1: internal audit; externally confirmed to be following recognized standards and/or good practices 

L2: onsite assessment following assessment process according to the respective standards

L3: According to process for EUCS Level High ; ad interim: see Label Level 2

Accepted Standards: if scope is matching : C5, TISAX, SOC2, SecNumCloud, ISO 27001, CSA


**Criterion P3.1.6 : Physical Security: Prevent unauthorised physical access and protect against theft, damage, loss and outage of operations.**

BC: mandatory declaration

L1: mandatory declaration and external review

L2, L3: mandatory certification

Assessing Entity: 

L1: internal + authorized entity according to the EUCS Level Basic; ad interim: internal+ external confirmation that the internal audit followed recognized standards and/or good practices 

L2: Assessing entity authorized according to the respective standards

L3: Assessing entity authorized according to the respective standards

Assessment Process: 

L1: internal audit; externally confirmed to be following recognized standards and/or good practices 

L2: onsite assessment following assessment process according to the respective standards

L3: According to process for EUCS Level High ; ad interim: see Label Level 2

Accepted Standards: if scope is matching: C5, TISAX, SOC2, SecNumCloud, ISO 27001, CSA


**Criterion P3.1.7 : Operational Security: Ensure proper and regular operation, including appropriate measures for planning and monitoring capacity, protection against malware, logging and monitoring events, and dealing with vulnerabilities, malfunctions and failures.**

BC: mandatory declaration

L1: mandatory declaration and external review

L2, L3: mandatory certification

Assessing Entity: 

L1: internal + authorized entity according to the EUCS Level Basic; ad interim: internal+ external confirmation that the internal audit followed recognized standards and/or good practices 

L2: Assessing entity authorized according to the respective standards

L3: Assessing entity authorized according to the respective standards

Assessment Process: 

L1: internal audit; externally confirmed to be following recognized standards and/or good practices 

L2: onsite assessment following assessment process according to the respective standards

L3: According to process for EUCS Level High ; ad interim: see Label Level 2

Accepted Standards: if scope is matching: C5, TISAX, SOC2, SecNumCloud, ISO 27001, CSA


**Criterion P3.1.8 : Identity, Authentication and access control management: Limit access to information and information processing facilities.**

BC: mandatory declaration

L1: mandatory declaration and external review

L2, L3: mandatory certification

Assessing Entity: 

L1: internal + authorized entity according to the EUCS Level Basic; ad interim: internal+ external confirmation that the internal audit followed recognized standards and/or good practices 

L2: Assessing entity authorized according to the respective standards

L3: Assessing entity authorized according to the respective standards

Assessment Process: 

L1: internal audit; externally confirmed to be following recognized standards and/or good practices 

L2: onsite assessment following assessment process according to the respective standards

L3: According to process for EUCS Level High ; ad interim: see Label Level 2

Accepted Standards: if scope is matching: C5, TISAX, SOC2, SecNumCloud, ISO 27001, CSA


**Criterion P3.1.9 : Cryptography and Key management: Ensure appropriate and effective use of cryptography to protect the confidentiality, authenticity or integrity of information.**

BC: mandatory declaration

L1: mandatory declaration and external review

L2, L3: mandatory certification

Assessing Entity: 

L1: internal + authorized entity according to the EUCS Level Basic; ad interim: internal+ external confirmation that the internal audit followed recognized standards and/or good practices 

L2: Assessing entity authorized according to the respective standards

L3: Assessing entity authorized according to the respective standards

Assessment Process: 

L1: internal audit; externally confirmed to be following recognized standards and/or good practices 

L2: onsite assessment following assessment process according to the respective standards

L3: According to process for EUCS Level High ; ad interim: see Label Level 2

Accepted Standards: if scope is matching: C5, TISAX, SOC2, SecNumCloud, ISO 27001, CSA


**Criterion P3.1.10 : Communication Security: Ensure the protection of information in networks and the corresponding information processing systems.**

BC: mandatory declaration

L1: mandatory declaration and external review

L2, L3: mandatory certification

Assessing Entity: 

L1: internal + authorized entity according to the EUCS Level Basic; ad interim: internal+ external confirmation that the internal audit followed recognized standards and/or good practices 

L2: Assessing entity authorized according to the respective standards

L3: Assessing entity authorized according to the respective standards

Assessment Process: 

L1: internal audit; externally confirmed to be following recognized standards and/or good practices (EUCS Basic(CKM-03.1, CKM-04.1))

L2: onsite assessment following assessment process according to the respective standards (EUCS Substantial(CKM-03.2, CKM-03.3, CKM-04.2, CKM-04.4))

L3: According to process for EUCS Level High ; ad interim: see Label Level 2 (EUCS High(CKM-03.4, CKM-04.3))

Accepted Standards: if scope is matching: C5, TISAX, SOC2, SecNumCloud, ISO 27001, CSA


**Criterion P3.1.11 : Portability and Interoperability: The CSP shall provide a means by which a customer can obtain their stored customer data, and provide documentation on how (where appropriate, through documented API’s) the CSC can obtain the stored data at the end of the contractual relationship and shall document how the data will be securely deleted from the Cloud Service Provider in what timeframe.**

Remark: this objective should be understood in the context of cybersecurity. Further portability objectives are defined in criteria 52 and 53.

BC: mandatory declaration

L1: mandatory declaration and external review

L2, L3: mandatory certification

Assessing Entity: 

L1: internal + authorized entity according to the EUCS Level Basic; ad interim: internal+ external confirmation that the internal audit followed recognized standards and/or good practices 

L2: Assessing entity authorized according to the respective standards

L3: Assessing entity authorized according to the respective standards

Assessment Process: 

L1: internal audit; externally confirmed to be following recognized standards and/or good practices 

L2: onsite assessment following assessment process according to the respective standards

L3: According to process for EUCS Level High ; ad interim: see Label Level 2

Accepted Standards: if scope is matching: C5, TISAX, SOC2, SecNumCloud, ISO 27001, CSA


**Criterion P3.1.12 : Change and Configuration Management: Ensure that changes and configuration actions to information systems guarantee the security of the delivered cloud service.**

BC: mandatory declaration

L1: mandatory declaration and external review

L2, L3: mandatory certification

Assessing Entity: 

L1: internal + authorized entity according to the EUCS Level Basic; ad interim: internal+ external confirmation that the internal audit followed recognized standards and/or good practices 

L2: Assessing entity authorized according to the respective standards

L3: Assessing entity authorized according to the respective standards

Assessment Process: 

L1: internal audit; externally confirmed to be following recognized standards and/or good practices 

L2: onsite assessment following assessment process according to the respective standards

L3: According to process for EUCS Level High ; ad interim: see Label Level 2

Accepted Standards: if scope is matching: C5, TISAX, SOC2, SecNumCloud, ISO 27001, CSA


**Criterion P3.1.13 : Development of Information systems: Ensure information security in the development cycle of information systems.**

BC: mandatory declaration

L1: mandatory declaration and external review

L2, L3: mandatory certification

Assessing Entity: 

L1: internal + authorized entity according to the EUCS Level Basic; ad interim: internal+ external confirmation that the internal audit followed recognized standards and/or good practices 

L2: Assessing entity authorized according to the respective standards

L3: Assessing entity authorized according to the respective standards

Assessment Process: 

L1: internal audit; externally confirmed to be following recognized standards and/or good practices 

L2: onsite assessment following assessment process according to the respective standards

L3: According to process for EUCS Level High ; ad interim: see Label Level 2

Accepted Standards: if scope is matching: C5, TISAX, SOC2, SecNumCloud, ISO 27001, CSA


**Criterion P3.1.14 : Procurement Management: Ensure the protection of information that suppliers of the CSP can access and monitor the agreed services and security requirements.**

BC: mandatory declaration

L1: mandatory declaration and external review

L2, L3: mandatory certification

Assessing Entity: 

L1: internal + authorized entity according to the EUCS Level Basic; ad interim: internal+ external confirmation that the internal audit followed recognized standards and/or good practices 

L2: Assessing entity authorized according to the respective standards

L3: Assessing entity authorized according to the respective standards

Assessment Process: 

L1: internal audit; externally confirmed to be following recognized standards and/or good practices 

L2: onsite assessment following assessment process according to the respective standards

L3: According to process for EUCS Level High ; ad interim: see Label Level 2

Accepted Standards: if scope is matching: C5, TISAX, SOC2, SecNumCloud, ISO 27001, CSA


**Criterion P3.1.15 : Incident Management: Ensure a consistent and comprehensive approach to the capture, assessment, communication and escalation of security incidents.**

BC: mandatory declaration

L1: mandatory declaration and external review

L2, L3: mandatory certification

Assessing Entity: 

L1: internal + authorized entity according to the EUCS Level Basic; ad interim: internal+ external confirmation that the internal audit followed recognized standards and/or good practices 

L2: Assessing entity authorized according to the respective standards

L3: Assessing entity authorized according to the respective standards

Assessment Process: 

L1: internal audit; externally confirmed to be following recognized standards and/or good practices 

L2: onsite assessment following assessment process according to the respective standards

L3: According to process for EUCS Level High ; ad interim: see Label Level 2

Accepted Standards: if scope is matching: C5, TISAX, SOC2, SecNumCloud, ISO 27001, CSA


**Criterion P3.1.16 : Business Continuity: Plan, implement, maintain and test procedures and measures for business continuity and emergency management.**

BC: mandatory declaration

L1: mandatory declaration and external review

L2, L3: mandatory certification

This criterion is consistent with criterion 60 (chapter European Control), which is more advanced in the case of Label Level 3.

Assessing Entity: 

L1: internal + authorized entity according to the EUCS Level Basic; ad interim: internal+ external confirmation that the internal audit followed recognized standards and/or good practices 

L2: Assessing entity authorized according to the respective standards

L3: Assessing entity authorized according to the respective standards

Assessment Process: 

L1: internal audit; externally confirmed to be following recognized standards and/or good practices 

L2: onsite assessment following assessment process according to the respective standards

L3: According to process for EUCS Level High ; ad interim: see Label Level 2

Accepted Standards: if scope is matching: C5, TISAX, SOC2, SecNumCloud, ISO 27001, CSA

**Criterion P3.1.17 : Compliance: Avoid non-compliance with legal, regulatory, self-imposed or contractual
information security and compliance requirements.**

BC: mandatory declaration

L1: mandatory declaration and external review

L2, L3: mandatory certification

Assessing Entity: 

L1: internal + authorized entity according to the EUCS Level Basic; ad interim: internal+ external confirmation that the internal audit followed recognized standards and/or good practices 

L2: Assessing entity authorized according to the respective standards

L3: Assessing entity authorized according to the respective standards

Assessment Process: 

L1: internal audit; externally confirmed to be following recognized standards and/or good practices 

L2: onsite assessment following assessment process according to the respective standards

L3: According to process for EUCS Level High ; ad interim: see Label Level 2

Accepted Standards: if scope is matching: C5, TISAX, SOC2, SecNumCloud, ISO 27001, CSA


**Criterion P3.1.18 : User documentation: Provide up-to-date information on the secure configuration and known vulnerabilities of the cloud service for cloud customers.**

BC: mandatory declaration

L1: mandatory declaration and external review

L2, L3: mandatory certification

Assessing Entity: 

L1: internal + authorized entity according to the EUCS Level Basic; ad interim: internal+ external confirmation that the internal audit followed recognized standards and/or good practices 

L2: Assessing entity authorized according to the respective standards

L3: Assessing entity authorized according to the respective standards

Assessment Process: 

L1: internal audit; externally confirmed to be following recognized standards and/or good practices 

L2: onsite assessment following assessment process according to the respective standards

L3: According to process for EUCS Level High ; ad interim: see Label Level 2

Accepted Standards: if scope is matching: C5, TISAX, SOC2, SecNumCloud, ISO 27001, CSA


**Criterion P3.1.19 : Dealing with information requests from government agencies: Ensure appropriate handling of government investigation requests for legal review, information to cloud customers, and limitation of access to or disclosure of Customer Data.**

BC: mandatory declaration

L1: mandatory declaration and external review

L2, L3: mandatory certification

Assessing Entity: 

L1: internal + authorized entity according to the EUCS Level Basic; ad interim: internal+ external confirmation that the internal audit followed recognized standards and/or good practices 

L2: Assessing entity authorized according to the respective standards

L3: Assessing entity authorized according to the respective standards

Assessment Process: 

L1: internal audit; externally confirmed to be following recognized standards and/or good practices 

L2: onsite assessment following assessment process according to the respective standards

L3: According to process for EUCS Level High ; ad interim: see Label Level 2

Accepted Standards: if scope is matching: C5, TISAX, SOC2, SecNumCloud, ISO 27001, CSA


**Criterion P3.1.20 : Product safety and security: Provide appropriate mechanisms for cloud customers to enable product safety and security.**

BC: mandatory declaration

L1: mandatory declaration and external review

L2, L3: mandatory certification

Assessing Entity: 

L1: internal + authorized entity according to the EUCS Level Basic; ad interim: internal+ external confirmation that the internal audit followed recognized standards and/or good practices 

L2: Assessing entity authorized according to the respective standards

L3: Assessing entity authorized according to the respective standards

Assessment Process: 

L1: internal audit; externally confirmed to be following recognized standards and/or good practices 

L2: onsite assessment following assessment process according to the respective standards

L3: According to process for EUCS Level High ; ad interim: see Label Level 2

Accepted Standards: if scope is matching: C5, TISAX, SOC2, SecNumCloud, ISO 27001, CSA


### Portability

The section refers to the application of Art. 6 (1) Free Flow of Data Regulation (FFoDR). It applies to any Service Offering, regardless of its Provider, type, purpose, or processed category of data.

#### Switching and porting of Customer Data

**Criterion P4.1.1 : The Provider shall implement practices for facilitating the switching of Providers and the porting of Customer Data in a structured, commonly used and machine-readable format including open standard formats where required or requested by the Customer.**

Note: The switching process involves three parties, being the Customer, the exiting Provider and the receiving Provider who should all duly co-operate to execute the transfer.

Note: The Customer Data received by the Customer or the importing Provider could include configuration information as well as information about the software systems used for the Service Offering.

BC and L1 and L2: mandatory declaration

L3: mandatory certification

Assessing Entity: 

L1 & L2: Gaia-X AISBL or mandated entity 

L3: SWIPO-accredited CAB 

Assessment Process:

L1 & L2: self-verified through internal audit and signed Gaia-X Self-Declaration 

L3: SWIPO self-declaration

Accepted Standards: SWIPO IaaS, SaaS and merged code CoC

Point Of Reference Standards:
- SWIPO-IAAS-CODE-OF-CONDUCT-VERSION-2020-27-MAY-2020-V3.0 – TR04; TR05; DP02; DP03; DP04; DP05; DP07


**Criterion P4.1.2 : The Provider shall ensure pre-contractual information exists, with sufficiently detailed, clear and transparent information regarding the processes of Customer Data portability, technical requirements, timeframes and charges that apply in case a professional user wants to switch to another Provider or port Customer Data back to its own IT systems.**


BC and L1 and L2: mandatory declaration

L3: mandatory certification

Assessing Entity: 

L1 & L2: Gaia-X AISBL or mandated entity 

L3: SWIPO-accredited CAB 

Assessment Process: 

L1 & L2: self-verified through internal audit and signed Gaia-X Self-Declaration 

L3: SWIPO self-declaration (M)

Accepted Standards: SWIPO IaaS, SaaS merged code CoC

Point Of Reference Standards:
- SWIPO-IAAS-CODE-OF-CONDUCT-VERSION-2020-27-MAY-2020-V3.0 – TR03; PR01; PR02; PR03; PR04; PR05; PR06; PR07


### European Control

This section applies to any service offering, regardless of its Provider, type, purpose, or processed category of data. However, requirements shall only apply subject to the indicated labels. This section aims for addressing the Customer's or domain specific needs, e.g., by limiting storage and/or processing to the area of EU/EEA.

Gaia-X distinguishes 3 levels of Labels, starting from Label Level 1 (the lowest), up to Label Level 3 (the highest), which represent different degrees of compliance with regard to the goals of transparency, autonomy, data protection, security, interoperability, flexibility, and European Control. Some of the following requirements are specific to a respective Label Level.

#### Processing and storing of Customer Data in EU/EEA

**Criterion P5.1.1 : For Label Level 2, the Provider shall provide the option that all Customer Data are processed and stored exclusively in EU/EEA.**

This criterion is only required for Label Level 2.


BC and L1: not applicable

L2: mandatory declaration

L3: not applicable

Assessing Entity: Gaia-X AISBL or mandated entity

Assessment Process: self declaration until an external entity is accredited by the Gaia-x European Association for Data and Cloud AISBL

Accepted Standards: -

Point Of Reference Standards:
- option to have secnumcloud v3.2 19.2.b, 19.2.C, 19.2.D, 10.2.E


**Criterion P5.1.2 : For Label Level 3, the Provider shall process and store all Customer Data exclusively in the EU/EEA.**

This criterion is only required for Label Level 3.

BC and L1 and L2: not applicable

L3: mandatory declaration

Assessing Entity: Gaia-X AISBL or mandated entity

Assessment Process: self declaration until an external entity is accredited by the Gaia-x European Association for Data and Cloud AISBL

Accepted Standards: -

Point Of Reference Standards:
- SecNumCloud 3.2.a – 19.2.b; 19.2.c; 19.2.d; 19.2.e


**Criterion P5.1.3 : For Label Level 3, where the Provider or subcontractor is subject to legal obligations to transmit or disclose Customer Data on the basis of a non-EU/EEA statutory order, the Provider shall have verified safeguards in place to ensure that any access request is compliant with EU/EEA/Member State law.**

This criterion is only required for Label Level 3.

BC and L1 and L2: not applicable

L3: mandatory declaration

Note: this is a general principle which is not assessable. The verified safeguards are further specified in subsequent criteria in this section (P5.1.4 - P5.1.7).

**Criterion P5.1.4 : For Label Level 3, the Provider’s registered head office, headquarters and main establishment shall be established in a Member State of the EU/EEA.**

This criterion is required only for Label Level 3

BC and L1 and L2: not applicable

L3: mandatory declaration

Assessing Entity: Gaia-X AISBL or mandated entity

Assessment Process: self declaration until an external entity is acredited by the Gaia-x European Association for Data and Cloud AISBL

Accepted Standards: -


**Criterion P5.1.5 : For Label Level 3, Shareholders in the Provider, whose registered head office, headquarters and main establishment are not established in a Member State of the EU/EEA shall not, directly or indirectly, individually or jointly, hold control of the CSP. Control is defined as the ability of a natural or legal person to exercise decisive influence directly or indirectly on the CSP through one or more intermediate entities, de jure or de facto. (cf. Council Regulation No 139/2004 and Commission Consolidated Jurisdictional Notice under Council Regulation (EC) No 139/2004 for illustrations of decisive control).**

This criterion is required only for Label Level 3

BC and L1 and L2: not applicable

L3: mandatory declaration

Assessing Entity: Gaia-X AISBL or mandated entity

Assessment Process: self declaration until an external entity is accredited by the Gaia-x European Association for Data and Cloud AISBL

Accepted Standards: -


**Criterion P5.1.6 : For Label Level 3, in the event of recourse by the Provider, in the context of the services provided to the Customer, to the services of a third-party company - including a subcontractor - whose registered head office, headquarters and main establishment is outside of the European Union or who is owned or controlled directly or indirectly by another third-party company registered outside the EU/EEA, the third-party company shall have no access over the Customer Data nor access and identity management for the services provided to the Customer. The Provider, including any of its sub-processor, shall push back any request received from non-European authorities to obtain communication of Customer Data relating to European Customers, except if request is made in execution of a court judgment or order that is valid and compliant under Union law and applicable Member States law as provided by Article 48 GDPR.**

This criterion is required only for Label Level 3 

BC and L1 and L2: not applicable

L3: mandatory declaration

Assessing Entity: Gaia-X AISBL or mandated entity

Assessment Process: self declaration until an external entity is accredited by the Gaia-x European Association for Data and Cloud AISBL

Accepted Standards: -


**Criterion P5.1.7 : For Label Level 3, the Provider must maintain continuous operating autonomy for all or part of the services it provides. The concept of operating autonomy shall be understood as the ability to maintain the provision of the cloud computing service by drawing on the provider’s own skills or by using adequate alternatives**

This criterion is only required for Label Level 3 

BC and L1 and L2: not applicable

L3: mandatory declaration

Assessing Entity: Gaia-X AISBL or mandated entity

Assessment Process: self declaration until an external entity is accredited by the Gaia-x European Association for Data and Cloud AISBL

Accepted Standards: -


#### Access to Customer Data

**Criterion P5.2.1 : The Provider shall not access Customer Data unless authorized by the Customer or when the access is in accordance with applicable laws in scope of the legally binding act.**

Note: in Europe this refers to all applicable EU/EEA/Member State laws.

BC, L1, L2, L3: mandatory declaration

Assessing Entity: Gaia-X AISBL or mandated entity

Assessment Process: self-assessment

Accepted Standards: -


### Sustainability

**Criterion P6.1 :  The Provider shall provide transparency on the environmental impact of the Service Offering provided**

Note: transparency can be created via a link to an environmental impact report of the Provider. This may be an aggregate statement on a portfolio of services, not necesessarily the impact of an individual Service Offering. The report shall describe the consumption of natural resources such as water and energy sources, the carbon footprint, the use of polluants and other factors.

BC, L1, L2 and L3 : mandatory declaration

Verification Process: self-assessment

**Criterion P6.2 : The Provider shall ensure that the Service Offering meets or relies on an infrastructure Services Offering which meets a high standard in energy efficiency, meeting an annual target of PUE of 1.3 in cool climates and 1.4 in warm climates**

Note: By January 1, 2025 the metric should be met by any new data centre at full capacity used to provide the Service Offering. Pre-existing data centres will achieve these same targets by January 1, 2030. The targets apply to all data centres larger than 50KW of maximum IT power demand.

Assessing Entity: 

- L1: Gaia-X AISBL or mandated entity

- L2/L3: Accredited Conformity Assessment Body for one of the Accepted Standards

Assessment Process: 

- L1: legally binding statement towards Gaia-X via the Trust Framework to comply with the Gaia-X Labelling criterion or public registered adherence to one of the Accepted Standards.

- L2 / L3: Assessment process by an accredited Conformity Assessment Body of one of the Accepted Standards

Permissible Standards: 
* Climate Neutral Data Centre Pact (CNDCP)


**Criterion P6.3 : The Provider shall ensure that the Service Offering meets or relies on an infrastructure Services Offering for which electricity demand will be matched by 75% renewable energy or hourly carbon-free energy by 31st December 2025, and 100% by 31st December 2030.**

Assessing Entity: 

- L1: Gaia-X AISBL or mandated entity

- L2/L3: Accredited Conformity Assessment Body for one of the Accepted Standards

Assessment Process: 

- L1: legally binding statement towards Gaia-X via the Trust Framework to comply with the Gaia-X Labelling criterion or public registered adherence to one of the Accepted Standards.

- L2 / L3: Assessment process by an accredited Conformity Assessment Body of one of the Accepted Standards

Permissible Standards: 
* Climate Neutral Data Centre Pact (CNDCP)


**Criterion P6.4 : The Provider shall ensure that the Service Offering meets or relies on an infrastructure Services Offering that will meet a high standard for water conservation demonstrated though the application through the application of a location and source sensitive water usage effectiveness (WUE)target of 0.4 L/kWh in areas with water stress**

Note: By January 1, 2025 new data centres at full capacity in cool climates that use potable water will be designed to meet a maximum WUE of 0.4 L/kWh in areas with water stress.The limit for WUE can be modified based on climate, stress and water type to encourage the use of sustainable water sources for cooling.By December 31, 2040, existing data centres that replace a cooling system will meet the WUE target applied to new data centres.

Assessing Entity: 

- L1: Gaia-X AISBL or mandated entity

- L2/L3: Accredited Conformity Assessment Body for one of the Accepted Standards

Assessment Process: 

- L1: legally binding statement towards Gaia-X via the Trust Framework to comply with the Gaia-X Labelling criterion or public registered adherence to one of the Accepted Standards.

- L2 / L3: Assessment process by an accredited Conformity Assessment Body of one of the Accepted Standards

Permissible Standards: 
* Climate Neutral Data Centre Pact (CNDCP)

## Policy Rules and Label Criteria for Data Spaces
Next to the rules for cloud service providers in the previous section, we anticipate that additional rules will be defined for the participants in data spaces and data sharing ecosystems. This is currently work-in-progress. Relevant objectives and guidelines will be elaborated and provided in a future version of this policy rules and labelling document.


